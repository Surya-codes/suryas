# Introduction to Python Programming

Python is a versatile programming language with many use cases in a variety of different fields.
Where it really shines is it's simple syntax and ease of usability.

Python can be used in: back-end development; data science; artificial intelligence; and much more. It has broad range of standard libraries avialable for free. It is portable across operating systems unless OS specific dependency is involved.

Now let's write the traditional "Hello,World!" program

```python
print("Hello,World!")
```

*Result*
```
Hello,World!
```

# Table of contents

- [Introduction to Python Programming](#introduction-to-python-programming)
  - [Print Multiple Lines](#print-multiple-lines)
  - [More Print Stuff](#more-print-stuff)
    - [Can specify seperator between strings](#can-specify-seperator-between-strings)
    - [Use \ to escape illegal strings](#use--to-escape-illegal-strings)
    - [Using escape characters like `\n`, `\t`](#using-escape-characters-like-n-t)
  - [What's with the quotes](#whats-with-the-quotes)
  - [Variables](#variables)
      - [Rules for creating variables in Python:](#rules-for-creating-variables-in-python)
  - [Providing Input](#providing-input)
  - [Strings](#strings)
    - [Print multiple strings together](#print-multiple-strings-together)
    - [String indexing](#string-indexing)
    - [String Methods](#string-methods)
    - [replace a word](#replace-a-word)
    - [split a word](#split-a-word)
  - [String Formatting](#string-formatting)
    - [The .format() method](#the-format-method)
    - [f-string method](#f-string-method)
    - [Format Specifiers](#format-specifiers)
- [Operators](#operators)
  - [Arithmatic Operators](#arithmatic-operators)
  - [Comparison Operators](#comparison-operators)
  - [Logical Operators](#logical-operators)
    - [Truthy and Falsy](#truthy-and-falsy)
- [Importing Modules](#importing-modules)
  - [Basic Python Import](#basic-python-import)
  - [Aliased Python import](#aliased-python-import)
  - [Using from...import](#using-fromimport)

## Print Multiple Lines
Wrap the lines in triple quotes
```Python
print(
    """
Hello, World!
Welcome to your first lesson on Python.
Triple quotes allow to print multi-lines
"""
)
```
*Result*
```
Hello, World!
Welcome to your first lesson on Python.
Triple quotes allow to print multi-lines
```

## More Print Stuff
### Can specify seperator between strings
```Python
print("There are <", 2**32, "> possibilities!", sep="-") 
```
*Result*
```
>>> There are <-4294967296-> possibilities!
```
### Use \ to escape illegal strings
```Python
print("Bucky\\Winter Soldier") 
```
*Result*
```
Bucky\Winter Soldier
```
### Using escape characters like `\n`, `\t`
```Python
print("Nothing that we do, is done in vain.\nI believe, \
with all my soul, that we shall see triumph. \
\n\t- Charles Dickens, A Tale of Two Cities")
```
*Result*
```
Nothing that we do, is done in vain.
I believe, with all my soul, that we shall see triumph. 
    - Charles Dickens, A Tale of Two Cities
```
## What's with the quotes
```Python
print('This is a string with in Single Quotes')
```
*Result*
```
This is a string with in Single Quotes
```

```Python
print("This is a 'string' but with a single quotes nested in double quotes")
```
*Result*
```
This is a 'string' but with a single quotes nested in double quotes
```
```Python
print('This is a "string" but with a double quotes nested in single quotes')
```
*Result*
```
This is a "string" but with a double quotes nested in single quotes
```
```Python
print("""
Use triple quotes to print in multiple lines.
Like this one.
""")
```
*Result*
```
Use triple quotes to print in multiple lines.
Like this one.
```

## Variables
Python is not “statically typed”. We do not need to declare variables before using them or declare their type. 
- A variable is created the moment we first assign a value to it. 
- A variable is a name given to a memory location. It is the basic unit of storage in a program.
- The value stored in a variable can be changed during program execution.
- A variable is only a name given to a memory location, all the operations done on the variable effects that memory location.

#### Rules for creating variables in Python:
- A variable name must start with a letter or the underscore character.
- A variable name cannot start with a number.
- A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ ).
- Variable names are case-sensitive (name, Name and NAME are three different variables).
- The reserved words(keywords) cannot be used naming the variable.

```Python
name = "Red Guardian"  # string
age = 50  # integer
height = 1.88  # Floats
superhero = True  # Boolean
hydra = False  # Boolean
powers = ["Super Strength", "Artificially Enhanced Physiology"]  # List
abilities = ("Expert Combatant", "Shield Mastery")  # Tuple
known_relations = {"wife": "Melina", "daughters": ("Natasha", "Yelena")}  # Dictionary
```
## Providing Input
```Python
age_input = input("What is your age")
print(age_input)
```
## Strings
 Strings can be any length and can include any character such as letters, numbers, symbols, and whitespace (spaces, tabs, new lines)

 ```Python
 # Concatenate two strings
first_string = "Darth" #string
second_string = "Vader" #string
new_string = first_string + second_string
print(new_string)
 ```
*Result*
```
DarthVader
```

### Print multiple strings together
```python
print(first_string,second_string)
```
*Result*
```
DarthVader
```

### String indexing
```python
bookName = "Ron Weasly and the rat that isn't a rat"
print(bookName[0])
print(bookName[2])
print(bookName[0:3])
print(bookName[0 : len(bookName)])
print(bookName[0:])
print(bookName[: len(bookName) - 1])
print(bookName[:-1])
print(bookName[:-5])
print(bookName[0:9:2])  # step size
print(bookName[::-1])  # step size but in reverse
```
*Result*
```
R
n
Ron
Ron Weasly and the rat that isn't a rat
Ron Weasly and the rat that isn't a rat
Ron Weasly and the rat that isn't a ra
Ron Weasly and the rat that isn't a ra
Ron Weasly and the rat that isn't 
RnWal
tar a t'nsi taht tar eht dna ylsaeW noR
```
### String Methods
Python has a set of built-in methods that you can use on strings.
```python
# make uppercase
print('Luke'.upper())

# make lowercase
print('YODA'.lower())

# capitalize
print('obiwan Kenobi'.capitalize())

# title
print('din djarin'.title())
```
*Result*
```
LUKE
yoda
Obiwan kenobi
Din Djarin
```
### replace a word
```python
'hello world'.replace('world', 'Mars')
```
*Result*
```
'hello Mars'
```

### split a word
```python
'clone-wars'.split(sep = '-')
```
*Result*
```
['clone', 'wars']
```

## String Formatting
Python provides one or more string formatting functions that use a template string with placeholders and optional alignment, width, and precision indicators to generate formatted output. Let's look two of them.

```python
first_name = "Din"
last_name = "Djarin"
alias = "Mando"
age = 36
profession = "Professional Baby Protector"
affiliation = "Bounty Hunters Guild"
```
### The .format() method
```python

print(
    (
        "Hello, {first_name} {last_name}. You are {age}. "
        + "You are a {profession}. You were a member of {affiliation}."
    ).format(
        first_name=first_name,
        last_name=last_name,
        age=age,
        profession=profession,
        affiliation=affiliation,
    )
)
```
*Result*
```
Hello, Din Djarin. You are 36. You are a Professional Baby Protector. You were a member of Bounty Hunters Guild.
```
### f-string method
```python

print(f"Hello, {first_name} {last_name}. You are {age}. "
    + f"You are a {profession}. You were a member of {affiliation}.")
```
*Result*
```
Hello, Din Djarin. You are 36. You are a Professional Baby Protector. You were a member of Bounty Hunters Guild.
```
Another example
```python
name = "Bucky"
profession = "Soldier"
affiliation = "Hydra"
message = (
    f"Hi {name}.\n"
    f"You are a {profession}.\n"
    f"You were in {affiliation}."
)
print(message)
```
*Result*
```
Hi Bucky.
You are a Soldier.
You were in Hydra
```

### Format Specifiers

This is a sizebale topic and will only be covered on the surface level. For full documentation visit [formatspec](https://docs.python.org/3.6/library/string.html#formatspec)

**Template**
```
[fill]align][sign][#][0][width][grouping_option][.precision][type]
```
Variables for below examples
```python
total = 193456.78922222
debt = -32423.2299999999
```
**width, grouping_option, presicion, type**
```python
pretty = (f'{total:15,.2f} is your total.\n'
         f'{debt:15_.2f} is your debt.')
print(pretty+'\n')
```
*Result*
```
     193,456.79 is your total.
     -32_423.23 is your debt.
```
**fill, right-align, width, grouping_option, presicion, type**
```python
pretty1 = (f'{total:0>15,.2f} is your total.\n'
         f'{debt:0>15_.2f} is your debt.')
print(pretty1+'\n')
```
*Result*
```
00000193,456.79 is your total.
00000-32_423.23 is your debt.
```
**fill, left-align, width, grouping_option, presicion, type**
```python
pretty3 = (f'{total:0^15,.2f} is your total.\n'
         f'{debt:0^15_.2f} is your debt.')
print(pretty3+'\n')
```
*Result*
```
00193,456.79000 is your total.
00-32_423.23000 is your debt.
```

**fill, left-align, width, grouping_option, presicion, type**
```python
pretty3 = (f'{total:0^15,.2f} is your total.\n'
         f'{debt:0^15_.2f} is your debt.')
print(pretty3+'\n')
```
*Result*
```
00193,456.79000 is your total.
00-32_423.23000 is your debt.
```
**fill, padding(align), width, grouping_option, presicion, type**
```python
pretty4 = (f'{total:0=15,.2f} is your total.\n'
         f'{debt:0=15_.2f} is your debt.')
print(pretty4+'\n')
```
*Result*
```
0,000,193,456.79 is your total.
-000_032_423.23 is your debt.
```

**0, width, grouping_option, presicion, type**
```python
pretty5 = (f'{total:015,.2f} is your total.\n'
         f'{debt:015_.2f} is your debt.')
print(pretty5+'\n')
```
*Result*
```
0,000,193,456.79 is your total.
-000_032_423.23 is your debt.
```
**0, sign width, grouping_option, presicion, type**
```python
pretty6 = (f'{total:+015,.2f} is your total.\n'
         f'{debt:+015_.2f} is your debt.')
print(pretty6+'\n')
```
*Result*
```
+000,193,456.79 is your total.
-000_032_423.23 is your debt.  
```  

**fill, padding(part of align), 0, sign, width, grouping_option, presicion, type**

```
pretty7 = (f'{total: =+15,.2f} is your total.\n' # filled with space
         f'{debt: =+15_.2f} is your debt.') # filled with space
print(pretty7+'\n')
```
*Result*
```
+    193,456.79 is your total.
-     32_423.23 is your debt.
```

**Using `type` to print number in multiple number systems**
```
twentyOne = 21

print(f'Twenty One in decimal is {twentyOne}\n'
     f'Twenty One in binary is {twentyOne:#b}\n'
     f'Twenty One in octal is {twentyOne:#o}\n'
     f'Twenty One in hex is {twentyOne:#x}\n')
```
*Result*
```
Twenty One in decimal is 21
Twenty One in binary is 0b10101
Twenty One in octal is 0o25
Twenty One in hex is 0x15
```
**Print float to percent**
```python
percent = .898976

print(f'{percent:.2%}')
```
*Result*
```
89.90%
```
# Operators
## Arithmatic Operators
```python
a = 4
b = 3
print(+a)

print(-b)

print(a + b)

print(a - b)

print(a * b)

print(a / b) # The result of standard division (/) is always a float, even if the dividend is evenly divisible by the divisor

print(a % b)

print(a ** b)
```
*Result*
```
4
-3
7
1
12
1.3333333333333333
1
64
```
## Comparison Operators
```Python
a = 10
b = 20


print(a == b)

print(a != b)

print(a <= b)

print(a >= b)
```
*Result*
```
False
True
True
False
```
## Logical Operators
In Python, Logical operators are used on conditional statements (either True or False). They perform Logical AND, Logical OR and Logical NOT operations.  

| OPERATOR |DESCRIPTION|  SYNTAX |
|--------|-------|-------|
| and      | Logical AND: True if both the operands are true    | x and y |
| or       | Logical OR: True if either of the operands is true | x or y  |
| not      | Logical NOT: True if operand is false              | not x   |


```Python
a = 10
b = -5
  
if a < 0 or b < 0:
  print("Their product will be negative")
else:
  print("Their product will be positive")
```
*Result*
```
Their product will be negative
```
**Truth Table:**  

| Operator A | Operator B | Logical AND result |
|----------|----------|------------------|
| True       | True       | True               |
| True       | False      | False              |
| False      | True       | False              |
| False      | False      | False              |

### Truthy and Falsy
All values are considered "truthy" except for the following, which are "falsy":

- None
- False
- 0
- 0.0
- 0j
- decimal.Decimal(0)
- fraction.Fraction(0, 1)
- [] - an empty list
- {} - an empty dict
- () - an empty tuple
- '' - an empty str
- b'' - an empty bytes
- set() - an empty set
- an empty range, like range(0)
- objects for which
- obj.__bool__() returns False
- obj.__len__() returns 0

A "truthy" value will satisfy the check performed by if or while statements. We use "truthy" and "falsy" to differentiate from the bool values True and False.

# Importing Modules
In Python, `import` keyword is used make code in one module available in another. Imports in Python are important for structuring your code effectively. Using Imports will allows us to structure and maintain your code properly.

## Basic Python Import
```python
import math

pi_value = math.pi
square_root_of_pi = math.sqrt(pi_value)
piToThePowerOfSix = math.pow(pi_value, 6)
print(
    f"Pi_value is {pi_value:.2f}\n\
square root of Pi is {square_root_of_pi:.2f}\n\
Pi to the power of six is {piToThePowerOfSix:.2f}"
)
```
*Result
*
```
Pi_value is 3.14
square root of Pi is 1.77
Pi to the power of six is 961.39
```

## Aliased Python import
You can also rename modules and attributes as they’re imported:
```python
import math as m

pi_value = m.pi
square_root_of_pi = m.sqrt(pi_value)
piToThePowerOfSix = m.pow(pi_value, 6)
print(
    f"Pi_value is {pi_value:.2f}\n\
square root of Pi is {square_root_of_pi:.2f}\n\
Pi to the power of six is {piToThePowerOfSix:.2f}"
)
```
*Result*
```
Pi_value is 3.14
square root of Pi is 1.77
Pi to the power of six is 961.39
```

## Using from...import

```python
from math import *
pi_value = pi
square_root_of_pi = sqrt(pi_value)
piToThePowerOfSix = pow(pi_value, 6)
print(
    f"Pi_value is {pi_value:.2f}\n\
square root of Pi is {square_root_of_pi:.2f}\n\
Pi to the power of six is {piToThePowerOfSix:.2f}"
)
```
*Result*
```
Pi_value is 3.14
square root of Pi is 1.77
Pi to the power of six is 961.39
```
You can also import only the objects that are being used  
**CORRECTION**:  
In class it was stated that this improves performance but that is wrong. This loads entire module as well but code looks much cleaner
```python
from math import pi, sqrt, pow # No visible poerformance impact
pi_value = pi
square_root_of_pi = sqrt(pi_value)
piToThePowerOfSix = pow(pi_value, 6)
print(
    f"Pi_value is {pi_value:.2f}\n\
square root of Pi is {square_root_of_pi:.2f}\n\
Pi to the power of six is {piToThePowerOfSix:.2f}"
)
```
*Result*
```
Pi_value is 3.14
square root of Pi is 1.77
Pi to the power of six is 961.39
```

























# Lists, Tuples, Sets and Dictionaries

So far, we have worked with individual pieces of data like the string 'hello'. Then with variables we saw how to give this data a name. Now in this lesson, we'll see how we can group data together with collections like Lists, Tuples, Sets and Dictionaries.

- [Lists, Tuples, Sets and Dictionaries](#lists-tuples-sets-and-dictionaries)
  - [Lists](#lists)
    - [**Characteristics of Python Lists**](#characteristics-of-python-lists)
    - [**Lists are ordered**](#lists-are-ordered)
    - [**Lists can contain different data types**](#lists-can-contain-different-data-types)
    - [**List Elements Can Be Accessed by Index**](#list-elements-can-be-accessed-by-index)
    - [**Lists can be nested to arbitrary depth.**](#lists-can-be-nested-to-arbitrary-depth)
    - [**Lists are mutable**](#lists-are-mutable)
      - [**Changing single value**](#changing-single-value)
      - [**Changing Multiple List Values**](#changing-multiple-list-values)
      - [**Caution with mutability**](#caution-with-mutability)
    - [`list.append()`](#listappend)
    - [`list.remove()`](#listremove)
    - [`list.sort()`](#listsort)
    - [`sorted(list)`](#sortedlist)
    - [`list.extend()`](#listextend)
    - [`list.reverse()`](#listreverse)
  - [Tuples](#tuples)
    - [**Gotcha moment in tuples**](#gotcha-moment-in-tuples)
    - [**Tuple Unpacking**](#tuple-unpacking)
  - [Dictionaries](#dictionaries)
      - [**Accessing a value**](#accessing-a-value)
    - [**Get all the keys in dictionary**](#get-all-the-keys-in-dictionary)
    - [**We can add an entry**](#we-can-add-an-entry)
    - [**We can delete an entry**](#we-can-delete-an-entry)
    - [**Get all the values in the dictionary**](#get-all-the-values-in-the-dictionary)
    - [**Get all the items in the dictionary. Useful for loops.**](#get-all-the-items-in-the-dictionary-useful-for-loops)
    - [**Adding values to an empty dict**](#adding-values-to-an-empty-dict)
  - [Sets](#sets)
    - [Set Operations](#set-operations)

## Lists
A list in Python is used to store the sequence of various types of data.  
It is the most versatile datatype available in Python which can be written as a list of comma-separated values (items) between square brackets([]).

```python
# A Python list
revengers = ['Antonio Starch','Lebowski','Jolly Green','The Wizards','Manchurian Candidate']
```
### **Characteristics of Python Lists**

- Lists are ordered.
- Lists can contain different data types
- Lists can contain any arbitrary objects.
- List elements can be accessed by index.
- Lists can be nested to arbitrary depth.
- Lists are mutable.
- Lists are dynamic.

### **Lists are ordered**

A list is not merely a collection of objects. It is an ordered collection of objects. The order in which you specify the elements when you define a list is an innate characteristic of that list and is maintained for that list’s lifetime.

Lists that have the same elements in a different order are not the same
```python
[1,2,3,4] == [1,2,3,4]
```
*result*
```
True
```

```python
[1,2,3,4] == [2,1,4,3]
```
*result*
```
False
```

### **Lists can contain different data types**

```python
same_data_type = [1,2,3,4]
different_data_type = ['N7','Commander Shepard',234,True,2.4]
```
### **List Elements Can Be Accessed by Index**

Individual elements in a list can be accessed using an index in square brackets. This is exactly analogous to accessing individual characters in a string. List indexing is zero-based as it is with strings.

```python
revengers = ['Antonio Starch','Lebowski','Jolly Green','The Wizards','Manchurian Candidate']
print(revengers[0])
print(revengers[0:2])
print(revengers[-1])
print(revengers[1:4:2])
print(revengers[::-1])
print(revengers[:])
```
*Result*
```
Antonio Starch
['Antonio Starch', 'Lebowski']
Manchurian Candidate
['Lebowski', 'The Wizards']
['Manchurian Candidate', 'The Wizards', 'Jolly Green', 'Lebowski', 'Antonio Starch']
['Antonio Starch', 'Lebowski', 'Jolly Green', 'The Wizards', 'Manchurian Candidate']
```

### **Lists can be nested to arbitrary depth.**
A list can contain sublists, which in turn can contain sublists themselves, and so on to arbitrary depth.

```python
x = ['a', ['bb', ['ccc', 'ddd'], 'ee', 'ff'], 'g', ['hh', 'ii'], 'j']
print(x[1])
print(x[1][0])
print(x[3][0], x[3][1])
```

*Result*

```
['bb', ['ccc', 'ddd'], 'ee', 'ff']
bb
hh ii
```

### **Lists are mutable**
Once a list has been created, elements can be added, deleted, shifted, and moved around at will. Python provides a wide range of ways to modify lists.
#### **Changing single value**
```python
list_to_modify = ['N7','Commander Shepard',234,True,2.4]
list_to_modify[2] = 546
print(list_to_modify)
```
*Result*
```
['N7', 'Commander Shepard', 546, True, 2.4]
```

#### **Changing Multiple List Values**

```python
list_to_modify = ['N7','Commander Shepard',234,True,2.4]
list_to_modify[2:4] = ['Hunter','Gareth',False,234,678] # Note the element at index 4 will not be replaced
print(list_to_modify)
```

*Result*

```
['N7', 'Commander Shepard', 'Hunter', 'Gareth', False, 234, 678, 2.4]
```

#### **Caution with mutability**

Consider below code

```python
original = [1, 2, 3]
modified = original
modified[0] = 99
print(f'original: {original}, modified: {modified}')
```

*Result*

```
original: [99, 2, 3], modified: [99, 2, 3]
```

As you can it has modified the original list as well.You can get around this by creating new `list`.

```python
original = [1, 2, 3]
modified = list(original)  # Note list() 
# Alternatively, you can use copy method
# modified = original.copy()
modified[0] = 99
print(f'original: {original}, modified: {modified}')
```
*Result*
```
original: [1, 2, 3], modified: [99, 2, 3]
```

### `list.append()`

```python
revengers = ['Antonio Starch','Lebowski','Jolly Green','The Wizards','Manchurian Candidate']
revengers.append('Spider Ham')
print(revengers)
```

*Result*
```
['Antonio Starch', 'Lebowski', 'Jolly Green', 'The Wizards', 'Manchurian Candidate', 'Spider Ham']
```

### `list.remove()`

```python
my_list = ['Python', 'is', 'sometimes', 'fun']
my_list.remove('sometimes')
print(my_list)
```

*Result*
```
['Python', 'is', 'fun']
```

### `list.sort()`

```python
numbers = [45, 13, 62, 15, 11]
numbers.sort()
print(f'numbers: {numbers}')

numbers.sort(reverse=True)
print(f'numbers reversed: {numbers}')

words = ['this', 'is', 'a', 'list', 'of', 'words']
words.sort()
print(f'words: {words}')
```

*Result*
```
numbers: [11, 13, 15, 45, 62]
numbers reversed: [62, 45, 15, 13, 11]
words: ['a', 'is', 'list', 'of', 'this', 'words']
```

### `sorted(list)`
While `list.sort()` sorts the list in-place, `sorted(list)` returns a new list and leaves the original untouched:

```python
numbers = [8, 1, 6, 5, 10]
sorted_numbers = sorted(numbers)
print(f'numbers: {numbers}, sorted: {sorted_numbers}')
```
*Result*
```
numbers: [8, 1, 6, 5, 10], sorted: [1, 5, 6, 8, 10]
```

### `list.extend()`

```python
first_list = ['Longing', 'Rusted']
second_list = ['Seventeen','Daybreak' ,'Furnace']
first_list.extend(second_list)
print(f'first: {first_list}, second: {second_list}')
```
*Result*
```
first: ['Longing', 'Rusted', 'Seventeen', 'Daybreak', 'Furnace'], second: ['Seventeen', 'Daybreak', 'Furnace']
```

Alternatively you can also extend lists by summing them:

```python
first_list = ['Longing', 'Rusted']
second_list = ['Seventeen','Daybreak' ,'Furnace']
first_list += second_list  # same as: first_list = first_list + second_list
print(f'first: {first_list}')
```
*Result*
```
first: ['Longing', 'Rusted', 'Seventeen', 'Daybreak', 'Furnace']
```
```python
# If you need a new list
summed = first_list + second_list
print(f'summed: {summed}')
```
*Result*
```
summed: ['Longing', 'Rusted', 'Seventeen', 'Daybreak', 'Furnace', 'Seventeen', 'Daybreak', 'Furnace']
```

### `list.reverse()`

```python
revengers = ['Antonio Starch','Lebowski','Jolly Green','The Wizards','Manchurian Candidate']
revengers.reverse()
print(revengers)
```

*Result*
```
['Manchurian Candidate', 'The Wizards', 'Jolly Green', 'Lebowski', 'Antonio Starch']
```

## Tuples
Python provides another type that is an ordered collection of objects, called a tuple.
- Tuples are defined by enclosing the elements in parentheses (()).
- Tuples are similar to list but they are immutable hence they cannot be modified.

```python
prime_numbers_below_10 = (2,3,5,7)
```

You can use indexing methods used for strings ad lists for tuples

```python
prime_numbers_below_10 = (2,3,5,7)
print(prime_numbers_below_10[1])
```
*Result*
```
3
```
### **Tuples with single element**

```python
tuple1 = () # Empty tuple
not_tuple = (2) 
# This is just an integer not tuple. 
# Since () is used to define operator precedance this is evaluated just as an integer and not tuple 
tuple_with_one_element = (2,) # This a tuple
```
### **Tuple Unpacking**
When unpacking, the number of variables on the left must match the number of values in the tuple

```python
revengers = tuple(['Antonio Starch','Lebowski','Jolly Green','The Wizards','Manchurian Candidate'])
r1,r2,r3,r4,r5 = revengers
print(r1)
print(r2)
print(r3)
print(r4)
print(r5)
```
*Result*
```
Antonio Starch
Lebowski
Jolly Green
The Wizards
Manchurian Candidate
```

## Dictionaries

A dictionary consists of keys and values. It is helpful to compare a dictionary to a list. Instead of the numerical indexes such as a list, dictionaries have keys. These keys are used to access values within a dictionary. Dicts are created with {} with key:value painrs in them.

```python
# Create the dictionary

Dict = {"key1": 1, "key2": "2", "key3": [3, 3, 3], "key4": (4, 4, 4), ('key5'): 5, (0, 1): 6}
```
#### **Accessing a value**
```python
Dict["key3"]
```
*Result*
```
[3,3,3]
```
### **Get all the keys in dictionary**
```python
Dict.keys()
```
*Result*
```
dict_keys(['key1', 'key2', 'key3', 'key4', 'key5', (0, 1)])
```
### **We can add an entry**
```python
Dict['key7'] = '2007'
print('Dict')
```
*Result*
```
{'key1': 1, 'key2': '2', 'key3': [3, 3, 3], 'key4': (4, 4, 4), 'key5': 5, (0, 1): 6, 'key7': '2007'}
```
### **We can delete an entry**
```python
del(Dict['key3'])
print(Dict)
```
*Result*
```
{'key1': 1, 'key2': '2', 'key4': (4, 4, 4), 'key5': 5, (0, 1): 6, 'key7': '2007'}
```
### **Get all the values in the dictionary**
```python
Dict.values()
```
*Result*
```
dict_values([1, '2', (4, 4, 4), 5, 6, '2007'])
```
### **Get all the items in the dictionary. Useful for loops.**
```python
Dict.items()
```
*Result*
```
dict_items([('key1', 1), ('key2', '2'), ('key4', (4, 4, 4)), ('key5', 5), ((0, 1), 6), ('key7', '2007')])
```
### **Adding values to an empty dict**
```python
dict_for_items = {} # Empty Dict 
dict_for_items['Item1'] = 'Rambo'
print(dict_for_items)
```
*Result*
```
{'Item1': 'Rambo'}
```
## Sets
Sets are data structures, similar to lists or dictionaries. They are created using curly braces, or the set function.
```python
winterfell_Set = {'Ned','Catelyn','Robb','Arya','Sansa','Jon','Bran','Rickon'}
```
Some important notes about 'Sets'
- Sets differ from lists in several ways, but share several list operations such as `len`.
- They are unordered, which means that they can’t be indexed.
- They cannot contain duplicate elements.
- Instead of using append to add to a set, use add.
- The method remove removes a specific element from a set; pop removes an arbitrary element
```python
nums = {1, 2, 1, 3, 1, 4, 5, 6}
print(f'Orginal set is {nums}')
nums.add(-7) # We are adding -7 to the set
nums.remove(3) # We are removing 3 from the set
print(f'Modified set is {nums}')
```
*Result*
```
Orginal set is {1, 2, 3, 4, 5, 6}
Modified set is {1, 2, 4, 5, 6, -7}
```


### Set Operations
- The *union* operator `|` combines two sets to form a new one containing items in either.
- The *intersection* operator `&` gets items only in both.
- The *difference* operator `-` gets items in the first set but not in the second.
- The *symmetric difference* operator `^` gets items in either set, but not both.

```python
first = {2226091,2226092,2226093,2226094,2226095,2226096}
second = {2226091,2226093,3335673,4567843,2345346}

print(f'Union is {first | second}')
print(f'Intersection is {first & second}')
print(f'Difference between first and second is {first - second}')
print(f'Difference between second and first is {second - first}')
print(f'Symmetric difference is {first ^ second}')
```
*Result*
```

Union is {2345346, 4567843, 2226091, 2226092, 2226093, 2226094, 2226095, 2226096, 3335673}
Intersection is {2226091, 2226093}
Difference between first and second is {2226096, 2226092, 2226094, 2226095}
Difference between second and first is {3335673, 2345346, 4567843}
Symmetric difference is {2345346, 4567843, 2226092, 2226094, 2226095, 2226096, 3335673}
```
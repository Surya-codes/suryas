## Conditonals
Most, if not all the programming languages have flow control mechanisms that allow us to apply conditons in our code that determines whether pieces of code to be executed or not.

Let's look at some of the features of the Python language that allow us to perform conditional executions of our code.

---

### **The `if` statement**
The general Python syntax for a simple if statement is

```python
if condition: # This must be true to get the indented statement block below to get executed
    indentedStatementBlock
```

- If the condition is true, then do the indented statements.  
- If the condition is not true, then skip the indented statements.

```python
if True:
    print("This is true")   
```
> This is true

```python
if True:
    print("The code block inside 'if' statement")
    print("can have")
    print("multiple lines.")
```
> The code block inside 'if' statement  
> can have  
> multiple lines.  


```python
if False:
    print("This wont be printed since the condition is `False`") # No output
```
```python
if not False:
    print("not False is True")   
```
> not False is True
```python
if not True:
    print("not True is False") # No output
```
---

### **Comparison operators**

In the above examples, the `condition` in the `if` statements were all directly values that are either `True` or `False` (including the ones which used `not`)

For most of the practical scenarios, the `condition` is usually an `expression` that includes `comparisions`. Some examples of numerical comparisons are given here below:

```python
if 5 == 5:
    print("A double equal sign '==' compares the two sides of itself and gives the")
    print("result 'True' if the two sides have the same value")
```
> A double equal sign '==' compares the two sides of itself and gives the  
> result 'True' if the two sides have the same value  
```python
a = 5
if a == 5:
    print("== also works with variables")
```
> == also works with variables

```python
a = 5
if 5 == a:
    print("The variable can be on either side")
```
> The variable can be on either side

```python
a = 5
b = 5
if a == b:
    print("and == also works with two variables")
```
> and == also works with two variables
```python
a = 5
if a = 5: #syntax error
    print("This will give an error! A single = is an assignment operator, not a conditional test") 
    print("To check if a is equal to 5, you need to use the double equal sign ==")
```
> Error
>> if a = 5:  
>>        ^  
SyntaxError: invalid syntax
```python
a = 5
if a == 6:
    print("This will not be printed.") # no output
```

```python
a = 5
if 2*a == 10:
    print("You can also use mathematical expressions")
```
> You can also use mathematical expressions

In addition to the `==` operator, there are also several other comparison operators such as `<`, `>`, `>=`, `<=`, `!=`


---

### Logical operations

Python also allows you to build the `expression` out of logical combinations of several conditions using the keywords `and`, `or`, and `not`. The value of these operators is as follows
* `and` evaluates to True if both conditions are True
* `or` evaluates to True if either condition is True
* `not`evaluates to true if condition is not True. 
    - `not True` is `False`
    - `not False` is `True`  

**Below are few examples**

```python
if True and False: # not True
    print("This will not be printed") # no output
```

```python
if True or False: # True
    print("This will be printed")
```
> This will be printed
```python
if not 5 > 6:
    print("Will this be printed?")
```
> "Will this be printed?"

```python
if not 5 != 5:
    print("What about this?")
```
> What about this?

```python
if 5 < 6 and 10 > 9: 
    print("A mixture or conditional expressions and logical expressions")
```
> A mixture or conditional expressions and logical expressions

---

### [Operator Precedance](https://docs.python.org/3/reference/expressions.html#operator-precedence)
**Question:** Do you think that the statement `not False or True` evaluates to `True` or `False`? Try it out and see.

- To understand what happened , we have to know if Python first performs the operation `False or True` or if it performs the operation `not False` first. 
- The rules for which order python does things in can be found in the documentation for [Operator Precedance](https://docs.python.org/3/reference/expressions.html#operator-precedence). 
- In the example above, we can see that the `not` operator had precedence and python performed the `not` before it performed the `or`. 

What if we wanted to have python perform the `or` first? We can do this by enclosing `True  or False` in brackets:

```python
if not (False or True):
    print("not (False or True) is False so this will not be printed")
```

---

### The `elif` and `else` statements

- In Python, you can combine the `if` statement with `elif` and `else` commands in a chain in order to allow you to take actions in the case that the starting `if` statement was false. 

- `elif` (else if) is a command that allows you to check another condition only if the condition of the starting `if` is `False`. Note that if the `if` condition is `True`, the `elif` statement will be skipped and not checked.

- `else` is a command that allows you to execute some code if all of the `if` and all the `elif`s are are `False`.

- Note that to be part of the "chain", all the `elif`s and the last `else` must follow directly after each other's code blocks with no other code in between. A new `if` always starts a new chain. 
You can see how this works in the following examples:

```python
a = 5
if a < 6:
    print("a is less than 6")
else:
    print("a is not less than 6")
```
> a is less than 6

```python
a = 5
if a<6: 
    print("the 'if' statement found that a is less than 6")
elif a<6:
    print("this will never get printed!")
```
> the 'if' statement found that a is less than 6
```python
a = 5
if a<6: 
    print("the first if found that a is less than 6")
if a<6:
    print("unlike elif, a second if will get executed.")
```
> the first if found that a is less than 6  
> unlike elif, a second if will get executed.

Since the code inside your `if` code block is just regular code, you can also add another if statement inside that code block.  
This creates a <a href=https://en.wikipedia.org/wiki/Nesting_(computing)>nested</a> `if` statement inside your first one:

```python
# example of a nested if-statement
a=4
if a<6 and a>=0:
    if a>3:
        print("the value of a is 4 or 5")
    else:
        print("the value of a is 0, 1, 2, or 3")
else:
    print("none of these are the case")
```

---

## Loops

Loops are a construction in a programming language that allow you to execute the same piece of code repeatedly.

In Python there are two types of loops: `while` loops and `for` loops. We will look at `while` loops first and then move on to the more commonnly used `for` loops. 
### **While loops**
Python utilizes the `while` loop similarly to other popular languages.  The `while` loop evaluates a condition then executes a block of code if the condition is true. The block of code executes repeatedly until the condition becomes false.

**The basic syntax is:**

```python
counter = 0
while counter < 10:
    # Execute the block of code here as
    # long as counter is less than 10
    print(counter)
    counter += 1 # counter = counter + 1
```
> 0  
1  
2  
3  
4  
5  
6  
7  
8  
9  

**Another example:**
```python
days = 0    
week = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
while days < 7:
    print(f"Today is {week[days]}")
    days += 1
```

>Today is Monday  
Today is Tuesday  
Today is Wednesday  
Today is Thursday  
Today is Friday  
Today is Saturday  
Today is Sunday  

#### **Infinite Loops**:

As previously noted, a 'while' loop will run until the conditional logic is false. Because of this, it is important to set a "false" condition within the executable code. If no false is included, the while loop will run infinitely. Be cautious when setting logic parameters to prevent the infinite loop unless that is the desired output. 

Because `while` loops are conditioned controlled loops, they are great for programs that need to be run for an indefinite number of times. This allows for input to be taken time and time again until the condition is met. A good practical example would be for a game in which the player has the option to try again. Until the player responds with the response that meets the `while` loop condition, the player can continue to play the game, simulating the loop as many times as they please.

```python
Dormammu_did_not_accept_bargain = True
while Dormammu_did_not_accept_bargain:
    print("Dr.Strange: Dormammu! I've come to bargain")
    answer = input("Did Dormammu accept the bargain? (y=yes, n=no) ")
    if answer == 'n':
        print("YOU'VE COME TO DIE. YOUR WORLD IS MY WORLD")
    elif answer == 'y':
        Dormammu_did_not_accept_bargain = False     # end the while loop
    else:
        print("Enter y or n next time.")
print("Dormammu accepted the bargain") # if answer is 'y'

```

---

### **for Loops**

Python utilizes a for loop to iterate over a list of elements. This is different to C or Java, which use the for loop to change a value in steps and access something such as an array using that value.

For loops iterate over collection based data structures like `lists`, `tuples`, `sets`, `dictionaries` etc.

#### **Basic Syntax:**

```python
greetings = ['hello', 'hi', 'how are you doing', 'im fine', 'how about you']

for greeting in greetings:
    print(thing)
```
> hello  
hi  
how are you doing  
im fine  
how about you

#### **`for` loop on `tuples`**

```python
list_of_tuples = [(1,2), (3,4)]

for a, b in list_of_tuples:
    print("a:", a, "b:", b)
```

> a: 1 b: 2  
a: 3 b: 4

#### **`for` loop on strings**

```python
for character in 'Python':
    print(f"Give me a '{character}'!")
```



>  Give me a 'P'!  
Give me a 'y'!  
Give me a 't'!  
Give me a 'h'!  
Give me a 'o'!  
Give me a 'n'!  


#### Loops on a range function:

**Iterate over the range() function**

```python
for i in range(10):
    print(i)
```
- Apart from being a function, range is actually an immutable sequence type.  
- The output will contain results from lower bound i.e 0 to the upper bound i.e 10 but excluding 10.  
- By default, the lower bound or the starting index is set to zero.

> 0  
1  
2  
3  
4  
5  
6  
7  
8  
9  

Additionally, one can specify the lower bound of the sequence and even the step of the sequence by adding a second and a third parameter.

```python
for i in range(4,10,2): #From 4 to 9 using a step of two
    print(i)
```

> 4  
6  
8  

#### **Iterate over keys in a dictionary**

```python
fruits_to_colors = {"apple": "#ff0000",
                    "lemon": "#ffff00",
                    "orange": "#ffa500"}

for key in fruits_to_colors:
    print(key, fruits_to_colors[key])
```
> apple #ff0000  
lemon #ffff00  
orange #ffa500

#### **Iterate over a list and get the corresponding index with the enumerate() function**

```python
A = ["this", "is", "something", "fun"]

for index,word in enumerate(A):
    print(index, word)
```
>0 this  
1 is  
2 something  
3 fun  


#### **Iterating over a dictionary:**

```python
contacts = dict([('Bobby','7698'),('Frankie','3456'),('Jodie','6745')])
for name, phonenumber in contacts.items():
    print(name, "is reachable under", phonenumber)
```
> Bobby is reachable under 7698  
Frankie is reachable under 3456  
Jodie is reachable under 6745  

```python
for index, item in enumerate(contacts):
    print("Item", index, "is a", item)
```
>  Item 0 is a Bobby  
Item 1 is a Frankie  
Item 2 is a Jodie
---
#### **Nested Loops**

A nested loop is a loop inside a loop.

The "inner loop" will be executed one time for each iteration of the "outer loop":

```python
var = ["Hey", "How are you"]
name = ["Rick", "Morty"]
for x in var:
    for y in name:
        if x == 'Hey':
            print(x, y+'!')
        else:
            print(x,y+'?')
```


> Hey Rick!  
Hey Morty!  
How are you Rick?  
How are you Morty?  
---
## Using `else` in loops
Python permits you to use else with loops. The else case is executed when none of the conditions within the loop were satisfied. To use the else we have to make use of `break` statement so that we can break out of the loop on a satisfied condition. If we do not break out then the else part will be executed.

```python
# for else loop
nums = [12,16,18,21,26]

for num in nums:
    if num % 5 == 0:
        print(num)
        break
else: # no break
    print('Not found')
```
> Not found  

```python
i =1
while i <= 5:
    print(i)
    i += 1
    if i == 3:
        break
else: # no break
    print('Hit the While/Else Statement')
```
Where would you use for/else, while/else loops?

```python
to_search = ['John','Travis','Washington']
target = 'Washington'
for i, value in enumerate(to_search):
    if value == target:
        print(f'Found at index location {i}')
        break
else: # no break
        print("Not Found")
```
> Found at index location 2
---
## `break` and `continue`

In Python `break` and `continue` statements in can be used to alter the flow of a normal loop.

### **`break` Statement**
- The `break` statement is used to terminate the loop completely.
- The control of the program flows to the statement immediately after the body of the loop.

```python
for item in range(1, 6):
    print(item)
    break
```

> 1

Here, in the first iteration, the value of `item` is **1**. This is printed by the `print()` function.

When the `break` statement is encountered, the loop immediately ends, so nothing else gets printed.

If `break` was used in front of the `print()` statement, the loop would have terminated immediately without printing anything.

```python
for item in range(1, 6):
    break
    print(item)
```

---

`break` statements are almost always used inside decision-making statements like `if...else` to end the loop only when a certain condition is met.


### Using `break` Statement with `for`

```python
for item in range(1, 6):
    if item == 3:
        break
    print(item)

print("The end")
```

> 1   
2  
The end
---

### Using `break` with `while`

Let's create a program that prints number entered by the user, but terminates once the user enters a negative number.

```python
while True:
    number = float(input("Enter a number: "))
    if number < 0:
        break
    print("You entered:", number)
```

*Result*


>Enter a number: 4  
You entered: 4.0  
Enter a number: 67  
You entered: 67.0  
Enter a number: -9  


Here, the program terminates as soon as the user enteres a negative number.

---


## Python `continue` Statement
The `continue` statement in Python skips the rest of the code inside the loop for that iteration.

The loop will not terminate but continues on with the next iteration.

---

### Using `continue` with `for`

```python
for i in range(5):
    number = float(input("Enter a number: "))

    # check if number if negative
    if number < 0:
        continue

    print(number)

```  

*Output*  

> Enter a number: 4  
You entered: 4.0  
Enter a number: 54  
You entered: 54.0  
Enter a number: -9  
Enter a number: 76  
You entered: 76.0  
Enter a number: 67  
You entered: 67.0


Here, the `continue` statement is used to skip the current iteration when the number entered by the user is negative.

Unlike `break`, `continue` does not terminate the loop entirely, the loop runs specified number of times (5 in this case).

---
## Exercise-1
Consider this set: `set([17, 72, 97, 8, 32, 15, 63, 57, 60, 83, 48, 100, 26, 12, 62, 3, 49, 55, 77, 98, 0, 89, 34, 92, 29, 75, 13, 40, 2, 69, 1, 87, 27, 54, 67, 28, 56, 70, 44, 86, 58, 37, 53, 71, 82, 23, 80, 95, 42, 91, 64, 85, 24, 38, 36, 50, 4, 61, 31, 51, 22, 46, 99, 94, 47, 11])` 

Write a Python program to read this set:
- Put even numbers in a list `even_list` and print out the count of even numbers and the list as shown below
- Put odd numbers in a list `odd_list` and print out the count of odd numbers and the list as shown below
- The output should look like this
```
There are 34 even numbers in the given set. Here is the list:  
[0, 2, 4, 8, 12, 22, 24, 26, 28, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 54, 56, 58, 60, 62, 64, 70, 72, 80, 82, 86, 92, 94, 98, 100]  
--------------------------------------------------  
There are 32 odd numbers in the given set. Here is the list:
[1, 3, 11, 13, 15, 17, 23, 27, 29, 31, 37, 47, 49, 51, 53, 55, 57, 61, 63, 67, 69, 71, 75, 77, 83, 85, 87, 89, 91, 95, 97, 99]
```
You can find the solution [here](solutions/Solution-1.md)


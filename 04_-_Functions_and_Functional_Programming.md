# Functions, Functional Programming and Decorators

## Functions
Up until this point you must've encountered Python functions like `print()`, `input()` or `len()`.  
These functions have following characateristics:  

- They perform a specific task.
- They are reusable.
- They imporve the clarity of the code.
- They reduce the duplication of Code.
- They run only when they are called.  
  
Let's use some of the above characteristics to give a simple understanding for Functions.
> A function can be defined as an organized block of reusable code, which can be called whenever required.

Python allows us to divide a large program into smaller chunks using Functions. As a program grows, functions make the code base more organized.

Though Python provides users many built-in functions, users can create their own functions.  
**Syntax:**
```python
def my_function(parameters):  
    function_block  
    return expression
```  

### **Defining Functions**
You can define a new function using the `def` keyword.  
```python
def CharlesDickens():
    print('Have a heart that never hardens,')
    print('and a temper that never tires,')
    print('and a touch that never hurts.')
```
Note the round brackets or parentheses `()` and colon `:` after the function's name. Both are essential parts of the syntax. The function's *body* contains an *indented block of statements*. The statements inside a function's body are not executed when the function is defined. To execute the statements, we need to *call* or *invoke* the function.
```python
CharlesDickens()
```
*Output*
> Have a heart that never hardens,  
and a temper that never tires,  
and a touch that never hurts.  

#### **Function arguments**

Functions can accept zero or more values as *inputs* (also knows as *arguments* or *parameters*).
- Arguments help us write flexible functions that can perform the same operations on different values.
- Further, functions can return a result that can be stored in a variable or used in other expressions.

```python
def filter_even(number_list):
    result_list = []
    for number in number_list:
        if number % 2 == 0:
            result_list.append(number)
    return result_list

even_list = filter_even([1, 2, 3, 4, 5, 6, 7])
print(even_list)
```
*Output*
>[2,4,6]
### **Types of Arguments**

#### **Positional Arguments**
Positional arguments are the arguments that need to be passed to the function call in a proper order.

For example,

```python
def add_numbers(n1, n2):
    result = n1 + n2
    return result

result = add_numbers(5.4, 6.7)
print(result)
```

- Here, we have called the `add_numbers()` function with two arguments.

- These arguments we passed to the function are called positional arguments.

- It is because the first argument `5.4` is assigned to the first parameter `n1` and the second argument `6.7` is assigned to the second parameter `n2`. These arguments are passed based on their position.

If we remove the second argument, it will lead to an error message:

```python
def add_numbers(n1, n2):
    result = n1 + n2
    return result

result = add_numbers(5.4)
print(result)
```

**Output**

```
Traceback (most recent call last):
  File "<string>", line 5, in <module>
TypeError: add_numbers() missing 1 required positional argument: 'n2'
```

This error message is telling us that we need to pass a second argument during the function call because our add numbers function takes two arguments.

#### **Default Arguments**

Function arguments can have default values in Python.

We can provide a default value to an argument by using the assignment operator `=`. 

For example,

```python
def add_numbers(n1 = 100, n2 = 1000):
    result = n1 + n2
    return result

result = add_numbers(5.4)
print(result)
```

**Output**

```
1005.4
```

Here, `n1` takes the value passed in function call.

Since there is no second argument in the function call, `n2` takes the default value of `1000`.

If we run the function without any arguments:

```python
def add_numbers(n1 = 100, n2 = 1000):
    sum_num = n1 + n2
    return sum_num

result = add_numbers()
print(result)
```

**Output**
```
1100
```


#### **Keyword Arguments**

In Python, we can also pass arguments using their parameter name.

```python
def greet(name, message):
    print("Hello", name)
    print(message)

greet("Jack", "What's going on?")

greet(message = "Howdy?", name = "Jill")
```

**Output**

```
Hello Jack
What's going on?
Hello Jill
Howdy?
```

During the second function call, we have passed arguments by their parameter name.

Python allows functions to be called using keyword arguments. When we call functions in this way, the order (position) of the arguments can be changed.


#### **Important note**
As of Python 3.8, function parameters can also be declared positional-only, meaning the corresponding arguments must be supplied positionally and can’t be specified by keyword.

To designate some parameters as positional-only, you specify `/` in the parameter list of a function definition. Any parameters to the left of `/` must be specified positionally. For example, in the following function definition, x and y are positional-only parameters, but z may be specified by keyword:

```python
# This is Python 3.8
def f(x, y, /, z):
    print(f'x: {x}')
    print(f'y: {y}')
    print(f'z: {z}')
# This is a valid call
f(1, 2, 3)

# This is also a valid call
f(1,2,z=3)
```

### **Variable Length Arguments** `*args` 

A variable length argument `*args` as the name suggests is an argument that can accept variable number of values.  
Variable arguments help in the scenario where the exact number of arguments are not known in the beginning, it also helps to make your function more flexible. Consider the scenario where you have a function to add numbers.

***Consider the scenario where you have a function to add numbers.***
```python
def add_num(num1, num2):
    return num1 + num2
```
This function can only be used to add two numbers, if you pass more than two numbers you will get an error.

By changing the argument to `*args` you can specify that function accepts variable number of arguments and can be used to sum ‘n’ numbers.

```python
def add_num(*args):
    sum_num = 0
    for num in args:
        sum_num += num
    return sum_num

result = add_num(5, 6, 7)
print('Sum is', result)

result = add_num(5, 6, 7, 8)
print('Sum is', result)

result = add_num(5, 6, 7, 8, 9)
print('Sum is', result)
```
*Output*
>Sum is 18  
Sum is 26  
Sum is 35  

Important notes about variable length arguments in Python:
- It is not mandatory to name variable length argument as `*args`. What is required is *, variable name can be any variable name for example *numbers, *names.
- Using variable length argument you can pass zero or more arguments to a function.
- Values pass to `*args` are stored in a tuple.

```python
def add_num(n, *numbers):
    sum_num = 0
    print('n is', n)
    for num in numbers:
        sum_num += num
    sum_num += n
    return sum_num

result = add_num(8, 5, 6, 7)
print('Sum is', result)
```
*Output*
```
n is 8
Sum is 26
```

### **Keyword variable length arguments in Python** `**kwargs`

Python keyword variable length argument is an argument that accept variable number of keyword arguments (arguments in the form of key, value pair). To indicate that the function can take keyword variable length argument you write an argument using double asterisk ‘**’, for example **kwargs.

Values passed as keyword variable length argument are stored in a dictionary that is referenced by the keyword arg name.

```python
def display_records(**records):
    for k, v in records.items():
        print('{} = {}'.format(k,v))

display_records(Firstname="Jack", Lastname="Douglas", marks1=45, marks2=42)
display_records(Firstname="Lisa", Lastname="Montana", marks1=48, marks2=45, marks3=49)

```
*Output*
```
Firstname = Jack
Lastname = Douglas
marks1 = 45
marks2 = 42
Firstname = Lisa
Lastname = Montana
marks1 = 48
marks2 = 45
marks3 = 49
```

More reading on `*args` and `**kwargs` is [here.](https://realpython.com/python-kwargs-and-args/)


### Ordering Arguments

**Simplified rule**

> params,*args,keyword and default arguments,**kwargs

Arguments in a Python function must appear in a specific order. This order is as follows:

- Postional arguments
- *args
- Keyword and Default arguments
- **kwargs  

So, if you’re working with both *args and **kwargs, you would use this code:

`def printOrder(*args, **kwargs):`

In addition, if you want to specify default values for certain arguments that may arise, you can use this code:

`def printOrder(coffee, *args, coffee_order="Espresso", **kwargs):`

This function expects to receive an argument called coffee, and the default value of any argument called coffee_order will be Espresso. However, you can specify as many arguments as you want with this function, because *args and **kwargs are used.

Below example illustrates this rule:

```python
def param_order(
    PositionalParam1,
    PositionalParam2,
    PositionalParam3,
    /,
    *args,
    KeywordParam1,
    KeywordParam2,
    KeywordParam3,
    DefaultParam1=10,
    DefaultParam2=10,
    **kwargs,
):
    print(PositionalParam1, PositionalParam2, PositionalParam3)
    print(args)
    print(KeywordParam1, KeywordParam2, KeywordParam3)
    print(DefaultParam1, DefaultParam2)
    print(kwargs)


param_order(
    1,
    2,
    3,
    4,
    5,
    6,
    KeywordParam1=7,
    KeywordParam2=8,
    KeywordParam3=9,
    DefaultParam1=10,
    DefaultParam2=11,
    kwargNo1=12,
    kwargNo2=13,
)
```
*Output*
>1 2 3  
(4, 5, 6)  
7 8 9  
10 11  
{'kwargNo1': 12, 'kwargNo2': 13}  

## Basics of Functional Programming

Functional programming is a style of programming that is characterized by short functions, lack of statements, and little reliance on variables.

Python provides features like lambda, filter, map, and reduce that can basically cover most of what we would need to know about Functional Programming.

### Lambda Expressions

`Lambda expressions` - also known as "anonymous functions" - allow us to create and use a function in a single line. They are useful when we need a short function that will be used only once.

They are mostly used in conjunction with the map, filter and the sort methods, which we will see later.

***Syntax:*** `lambda` arguments: expression

Let's write a function in Python, that will compute the value of `(x**2) + 2`.

```python
def f(x):
    '''
    Function to compute the value of (x**2) + 2
    '''
    return (x**2)+2


print(f(3)) # Returns 11
```
*Output*
> 9

Now let's write the same a `lambda` function.

```python
squared_plus_two = lambda x: (x**2)+2
print(squared_plus_two(3))
```
*Output*
>11

#### **`Lambda` expression with multiple inputs **

```python
# Calculating Harmonic Mean using lambda function
harmonic_mean = lambda x,y,z : 3/(1/x + 1/y + 1/z)**0.5
print(f'{harmonic_mean(8, 10, 1000):.2f}')
```

#### **`Lambda` expression without inputs**

```python
names = ["Han Solo", "Luke Skywalker","Darth Vader","Boba Fett","Din Djarin","Ashoka Tano"]

names.sort(key = lambda name: name.split(" ")[-1].lower())
print(names)
```
*Output*
> ['Din Djarin', 'Boba Fett', 'Luke Skywalker', 'Han Solo', 'Ashoka Tano', 'Darth Vader']

### The `map` function
Python `map()` applies a function on all the items of an iterable(list,tuple,set,dict,string,...) given as input and returns a map object.
**Syntax**
`map(function_to_apply, iterables)`

Let's see below script which calcuates area of a circle for eeach element in the radius list.

```python
from math import pi

listOfRadii = [1, 2, 3, 4, 5]

area = []


def areaOfCircle(radius):
    return round(pi*(radius**2), 2)


for radius in listOfRadii:
    area.append(areaOfCircle(radius=radius))
print(area)
```
*Output*
```
[3.14, 12.57, 28.27, 50.27, 78.54]
```

The above script can be rewrriten using map() as below:

```python
from math import pi

listOfRadii = [1, 2, 3, 4, 5]


def areaOfCircle(radius):
    return round(pi*(radius**2), 2)


area = list(map(areaOfCircle, listOfRadii))

print(area)
```

This can be further shortened using lambda:

```python
from math import pi

listOfRadii = [1, 2, 3, 4, 5]
areaOfCircle = lambda r: round(pi*(r**2), 2)

area = list(map(areaOfCircle,listOfRadii))

print(area)
```
*Output*
> [3.14, 12.57, 28.27, 50.27, 78.54]

### The `filter()` function

`filter()` can be used to select certain pieces of data from a list, tuple or other collection of data based on a condtion.

**Syntax**
`filter(function, iterable)`

Let's see an example where we want to get the list of all numbers that are greater than 99, from a given input list.

```Python
# Filter out all the numbers greater than 5 from a list

my_list = [99,100,156,567,234,88,34,23,12,34]
output_list = filter(lambda x : x>99, my_list)

print(list(output_list))
```
### **The `reduce()` method**

The 'reduce' function transforms a given list into a single value by applying a function cumulatively to the items of sequence, from left to right.

**Syntax**
`reduce(function,sequence[,initializer])`  

- Initializer is optional 

Below script uses reduce() to compute product of numbers in a list.

```python

from functools import reduce
product = reduce((lambda x,y : x*y), [1,2,3,4,5,6])

print(product)
```

Do note that in above example `initializer` was not specified.

**Let's look at below example which calculates number of occurances of even number.**

```python
values = [22, 4, 12, 43, 19, 71, 20]
# Using for loop, the imperative way
count = 0
for number in values:
    if number % 2 == 0:
        count += 1

print(count)
```
*Output*
> 4

**Let's rewrite this with `reduce()`**
```python
from functools import reduce
values = [22, 4, 12, 43, 19, 71, 20]
count = reduce(lambda count, num: count + 1 if num % 2 == 0 else count, values)
print(count) # Wrong answer
```
*Output*
> 25

As you see this is a wrong output. The reason is by default the initializer has chosen 22 as the initializer and started the `reduce()` operation. Let's correct the code by adding `0` for initializer.

```python
from functools import reduce
values = [22, 4, 12, 43, 19, 71, 20]
count = reduce(lambda count, num: count + 1 if num % 2 == 0 else count, values, 0)
print(count)
```
*Output*
> 4

**Let's create a dict from another dict using `reduce()`**

```python
from functools import reduce
list_of_invitees = [
    {"email": "sam@example.com", "name": "Sam", "status": "attending"},
    {"email": "timmy@example.com", "name": "Timothy", "status": "declined"},
    {"email": "kieth@example.com", "name": "Kieth", "status": "pending"},
    {"email": "richard@example.com", "name": "Richard", "status": "attending"},
    {"email": "bob@example.com", "name": "Bob", "status": "attending"}
]

# Let’s say you want to visualize the RSVP status of invitations by creating a dictionary as such:
# {'sam@example.com': 'attending', 'timmy@example.com': 'declined', 'kieth@example.com': 'pending', 'richard@example.com': 'attending', 'bob@example.com': 'attending'}


def transform_data(acc, invitee):
    acc[invitee["email"]] = invitee["status"]
    return acc


results = reduce(
    transform_data,
    list_of_invitees,
    {}  # Initializer
)
print(results)
```
*Output*
> {'sam@example.com': 'attending', 'timmy@example.com': 'declined', 'kieth@example.com': 'pending', 'richard@example.com': 'attending', 'bob@example.com': 'attending'}

### List Comprehensions

List comprehensions are a more easier way to define and create lists in Python. We can create lists with list comprehensions without worrying about initializing lists or setting up loops . It is a substitute for the lambda function as well as the functions map() and filter(). List comprhensions are more easier to read and understand.

Let's take a look at below code

```python
# Creating a list of squares of first 10 numbers using loops 

squares = []
for x in range(10):
    squares.append(x**2)
    
print(squares )
```
*Output*
> [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]


Now let's use list comprehension to achieve the same result in a one liner

```python
# Creating a list of squares of first 10 using list comprehension

squares = [x**2 for x in range(10)]
print(squares)
```
*Output*
> [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]

### List Comprehensions vs Map function
**`map()`**
```python
from math import pi

listOfRadii = [1, 2, 3, 4, 5]

area = []


def areaOfCircle(radius):
    return round(pi*(radius**2), 2)


for radius in listOfRadii:
    area.append(areaOfCircle(radius=radius))
print(area)
```

**List Comprehension**

```python
from math import pi

listOfRadii = [1, 2, 3, 4, 5]
area = [round(pi*(r**2), 2) for r in listOfRadii]

print(area)
```
### List Comprehensions vs Filter function
**`filter()`**
```Python
# Filter out all the numbers greater than 5 from a list

my_list = [99,100,156,567,234,88,34,23,12,34]
output_list = filter(lambda x : x>99, my_list)

print(list(output_list))
```
**List Comprehension**
```python
my_list = [99, 100, 156, 567, 234, 88, 34, 23, 12, 34]
output_list = [x for x in my_list if x > 99]

print(list(output_list))
```
### Dictionary comprehensions

```python
numbers = range(10)
new_dict_for = {}

# Add values to `new_dict` using for loop
for n in numbers:
    if n%2==0:
        new_dict_for[n] = n**2

print(new_dict_for)
```
*Output*
> {0: 0, 8: 64, 2: 4, 4: 16, 6: 36}

**Let's rewrite the above using Dictionary comprehension**
```python
numbers = range(10)

new_dict_comp = {n:n**2 for n in numbers if n%2 == 0}

print(new_dict_comp)
```

### `zip()`

The `zip()` function takes iterables (can be zero or more), aggregates them in a tuple, and return it.

**Syntax**
`zip(*iterables)`


**Example 1**

```python
number_list = [1, 2, 3]
str_list = ['one', 'two', 'three']

# No iterables are passed
result = zip()

# Converting iterator to list
result_list = list(result)
print(result_list)

# Two iterables are passed
result = zip(number_list, str_list)

# Converting iterator to set
result_set = set(result)
print(result_set)
```
*Output*
> []  
{(2, 'two'), (3, 'three'), (1, 'one')}

**Example2:**
```python
numbersList = [1, 2, 3]
str_list = ['one', 'two']
numbers_tuple = ('ONE', 'TWO', 'THREE', 'FOUR')

# Notice, the size of numbersList and numbers_tuple is different
result = zip(numbersList, numbers_tuple)

# Converting to set
result_set = set(result)
print(result_set)

result = zip(numbersList, str_list, numbers_tuple)

# Converting to set
result_set = set(result)
print(result_set)
```
*Output*
> {(2, 'TWO'), (3, 'THREE'), (1, 'ONE')}  
{(2, 'two', 'TWO'), (1, 'one', 'ONE')}

**Example3:**
```python
coordinate = ['x', 'y', 'z']
value = [3, 4, 5]

result = zip(coordinate, value)
result_list = list(result)
print(result_list)

c, v =  zip(*result_list)
print('c =', c)
print('v =', v)
```
*Output*
> [('x', 3), ('y', 4), ('z', 5)]  
c = ('x', 'y', 'z')  
v = (3, 4, 5)  

## Decorators

A very good reading on Decorators [here](https://www.freecodecamp.org/news/python-decorators-explained-with-examples/)

**Syntax**
```python
def my_decorator_func(func):

    def wrapper_func():
        # Do something before the function.
        func()
        # Do something after the function.
    return wrapper_func
# We use a decorator by placing the name of the decorator directly above the function we want to use it on. 
#You prefix the decorator function with an @ symbol
@my_decorator_func
def my_func():

    pass
```

**A Simple example**
```python
from functools import wraps
def addNumHelper(func):
    @wraps(func)
    def wrapper(*args,**kwargs):
        '''This is a wrapper Function'''
        print('This is before addNum function')
        result = func(*args,**kwargs)
        print('This is after addNum function')
        return result
    return wrapper
@addNumHelper
def addNum(a,b):
    '''
    This function adds numbers
    SYNTAX: addNum(a,b)
    '''
    print("This function adds numbers")
    
    return a+b
print(addNum(5,3))
```
*Output*
>This is before addNum function  
This function adds numbers  
This is after addNum function  
8




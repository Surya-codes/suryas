# File I/O

> **Note:** Examples in this notes use the current repository. Do download the repositiory and change the paths in examples as you please before running the code.

## The `os` module
Python allows developers to interface with Operating system through `os` module.

**Importing the module**
```python
import os
```

**Retrieving the path of the current working directory**
```python
print(os.getcwd())  # getcwd: get Current Working Directory
```
*Output in Windows* 
> C:\Users\xa392d\Desktop\



**To change your current working directory**
```python
os.chdir('C:/Users/xa392d/Desktop/python-notes/FileIO_practice/setup_dir') # chdir: change Directory
print(os.getcwd())
```
*Output*
> C:\Users\xa392d\Desktop\python-notes

**List Folders and Files**
```python
print(os.listdir()) # listdir: List Directory
```
*Output*
```
['.git',
 '.gitignore',
 '01 -  Strings, Variables and Operators.md',
 '02 - Lists, Tuples, Sets and Dictionaries.md',
 '03 - Conditionals and Loops.md',
 '04 - Functions and Functional Programming.md',
 '05 - File IO.md',
 'FileIO_practice',
 'solutions']
```

**Create a new Directory**
```
os.mkdir("hello_python")  # mkdir: Make Directory
os.makedirs("hello/python")
```

**Note:**

`makedirs()` creates all the intermediate directories if they don't exist.

`mkdir()` can create a single sub-directory and will throw an exception if intermediate directories that don't exist are specified.

 Either can be used to create a single 'leaf' directory (dirA):


```python
# Creating a directory called 'Engineering'
os.mkdir('Engineering')
```

```python
# Creating a directory called 'CMM' inside Techpubs directory
os.makedirs('Techpubs/CMM',exist_ok = True)
# This will create Techpubs folder and then it will create CMM folder.
# exist_ok = True will not throw error if folders already exist
```
**Delete a directory**
```python
os.rmdir("Engineering")  # Remove directory named "Engineering" in the current working directory
os.removedirs("Techpubs/CMM") # Remove the mentioned folder tree in the current working directory
```

**Rename a file**
```python
os.rename('test.txt', 'demo.txt') # syntax: os.rename(<older name>, <new name>)
```

**Details of a file**

```python
print(os.stat('demo.txt'))
modified_time = os.stat('demo.txt').st_mtime  # Modified time-stamp

from datetime import datetime

print(datetime.fromtimestamp(modified_time))
```
*Output*
```
os.stat_result(st_mode=33206, st_ino=5629499534400733, st_dev=303490849, st_nlink=1, st_uid=0, st_gid=0, st_size=0, st_atime=1626937945, st_mtime=1626937945, st_ctime=1626937945)
2021-07-22 12:42:25.688195

```

**Combining path names**

The `os.path.join()` method combines components in a path name to create a full path name.

This method makes it easy to combine two or more components of a path name.`os.path.join` automatically inserts forward slashes (`/`) into the path name when needed.

Check the below code to learn how `os.path.join()` is used to print paths of all the files in desktop

```python
import os

cwd = os.getcwd()
desktop = os.path.join(cwd, "FileIO_practice")

files = os.listdir(desktop)

for f in files:
	print(os.path.join(desktop, f))
```
*Output*
```
C:\Users\xa392d\Desktop\python-notes\FileIO_practice\.ipynb_checkpoints
C:\Users\xa392d\Desktop\python-notes\FileIO_practice\test.txt
C:\Users\xa392d\Desktop\python-notes\FileIO_practice\Untitled.ipynb
```

## `os.walk` method
OS.walk() generate the file names in a directory tree by walking the tree either top-down or bottom-up. 
For each directory in the tree rooted at directory top (including top itself), it yields a 3-tuple (dirpath, dirnames, filenames).

```python
import os
for path, dirnames, filenames in os.walk('C:/Users/xa392d/Desktop/python-notes'):
    if not r'C:/Users/xa392d/Desktop/python-notes\.git' in path: # Exclude .git directory, try without this if condition
        print(f'{path}\n {dirnames}\n {filenames}')
        print("-"*50+"\n")
```
*Output*
```
C:/Users/xa392d/Desktop/python-notes
 ['.git', '.ipynb_checkpoints', 'FileIO_practice', 'solutions']
 ['.gitignore', '01 -  Strings, Variables and Operators.md', '02 - Lists, Tuples, Sets and Dictionaries.md', '03 - Conditionals and Loops.md', '04 - Functions and Functional Programming.md', '05 - File IO.md', 'Untitled.ipynb']
--------------------------------------------------

C:/Users/xa392d/Desktop/python-notes\.ipynb_checkpoints
 []
 ['Untitled-checkpoint.ipynb']
--------------------------------------------------

C:/Users/xa392d/Desktop/python-notes\FileIO_practice
 ['.ipynb_checkpoints']
 ['test.txt', 'Untitled.ipynb']
--------------------------------------------------

C:/Users/xa392d/Desktop/python-notes\FileIO_practice\.ipynb_checkpoints
 []
 ['Untitled-checkpoint.ipynb']
--------------------------------------------------

C:/Users/xa392d/Desktop/python-notes\solutions
 []
 ['Solution-1.md']
--------------------------------------------------

```
## The `pathlib` module

`pathlib` module is more intuitive in terms of the syntax, easier to use and has more features out of the box.

**Showing Current Directory**

```python
from pathlib import Path
Path.cwd()
```
*Output*
> WindowsPath('C:/Users/xa392d/Desktop/python-notes')

**Check Directory or File Existing**

```python
Path('C:/Users/xa392d/Desktop/python-notes/.gitignore').exists()
```
*Output*
```
True
```

**Create a Directory**
```python
Path('test_dir').mkdir(exist_ok=True) # Doesn't raise error if already exists
```
**Creating Multi-level Depth Directory**
```python
Path(os.path.join('test_dir', 'level_1b', 'level_2b', 'level_3b')).mkdir(parents=True,exist_ok=True)
```
**Showing Directory Content with `.iterdir()`**
```python
from pathlib import Path

directory_content = Path('C:/Users/xa392d/Desktop/python-notes').iterdir()

for path in list(directory_content):
    print(path)
```
*Output*
```
C:\Users\xa392d\Desktop\python-notes\.git
C:\Users\xa392d\Desktop\python-notes\.gitignore
C:\Users\xa392d\Desktop\python-notes\01 -  Strings, Variables and Operators.md
C:\Users\xa392d\Desktop\python-notes\02 - Lists, Tuples, Sets and Dictionaries.md
C:\Users\xa392d\Desktop\python-notes\03 - Conditionals and Loops.md
C:\Users\xa392d\Desktop\python-notes\04 - Functions and Functional Programming.md
C:\Users\xa392d\Desktop\python-notes\05 - File IO.md
C:\Users\xa392d\Desktop\python-notes\demo.txt
C:\Users\xa392d\Desktop\python-notes\FileIO_practice
C:\Users\xa392d\Desktop\python-notes\solutions
C:\Users\xa392d\Desktop\python-notes\test_dir
```
**Using `glob` to generates lists of files matching given patterns**
```python
from pathlib import Path

directory_content = Path('C:/Users/xa392d/Desktop/python-notes').glob('*.md')

for path in list(directory_content):
    print(path)
```
*Output*
```
C:\Users\xa392d\Desktop\python-notes\01 -  Strings, Variables and Operators.md
C:\Users\xa392d\Desktop\python-notes\02 - Lists, Tuples, Sets and Dictionaries.md
C:\Users\xa392d\Desktop\python-notes\03 - Conditionals and Loops.md
C:\Users\xa392d\Desktop\python-notes\04 - Functions and Functional Programming.md
C:\Users\xa392d\Desktop\python-notes\05 - File IO.md
```
**Quick Read/Write to File**  
***Write***
```python
f = Path('test_dir/test.txt')
f.write_text('This is a sentence.') 
```
***Read***
```python
f.read_text()
```
**Metadata**
```python
print(f.resolve())
print(f.stem)
print(f.suffix)
f.stat()
```
*Output*
```
/Users/sam/Desktop/pythonNotes/04-Functions and Functional programming.md
04-Functions and Functional programming
.md
os.stat_result(st_mode=33188, st_ino=16993025, st_dev=16777233, st_nlink=1, st_uid=501, st_gid=20, st_size=19904, st_atime=1626689914, st_mtime=1626689912, st_ctime=1626689912)
```
## Writing and Saving text files in Python
`open()` function in Python allows us to read and write fiels.

*Syntax*
`open(filename, mode)`

- 'r' for reading
- 'w' for writing
- 'a' for  appending
- 'r+' for both reading and writing



**To write a dict to a text file**

```python
student_list = [{"Student ID": 1, "Gender": "F", "Name": "Emma"},
       {"Student ID": 2, "Gender": "M", "Name": "John"},
       {"Student ID": 3, "Gender": "F", "Name": "Linda"}]
with open(r'C:\Users\xa392d\Desktop\python-notes\FileIO_practice\dict_Write.txt','w') as writefile:
    for student in student_list:
        writefile.write("--------------\n",)
        for k,v in student.items():
            writefile.write(f"{k} {v}\n")
```
**To read a text file and print line by line**
```python
with open(r'C:\Users\xa392d\Desktop\python-notes\FileIO_practice\test.txt','r') as testwritefile:
    for line in testwritefile:
        print(line,"\n------------------")
```
*Output*
```
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non nulla vel quam ullamcorper pharetra at at dolor.
 
------------------
Ut ullamcorper, justo ultricies venenatis euismod, ligula mi elementum lacus, nec cursus risus libero quis purus. 

```
**Appending**
```python
student_list = [{"Student ID": 1, "Gender": "F", "Name": "Emma"},
       {"Student ID": 2, "Gender": "M", "Name": "John"},
       {"Student ID": 3, "Gender": "F", "Name": "Linda"}]
with open(r'C:\Users\xa392d\Desktop\python-notes\FileIO_practice\dict_Write.txt','w') as writefile:
    for student in student_list:
        writefile.write("--------------\n",)
        for k,v in student.items():
            writefile.write(f"{k} {v}\n")

with open(r'C:\Users\xa392d\Desktop\python-notes\FileIO_practice\dict_Write.txt','a') as writefile:
    writefile.write("\nEnd of the file folks")
```
**Writing a list to text file:**
```python
Lines=["This is line A\n","This is line B\n","This is line C\n"]

with open('Example2.txt','w') as writefile:
    for line in Lines:
        print(line)
        writefile.write(line)
```
**Writing to a CSV file**
```python
# Write CSV file example

student_list = [{"Student ID": 1, "Gender": "F", "Name": "Emma"},
       {"Student ID": 2, "Gender": "M", "Name": "John"},
       {"Student ID": 3, "Gender": "F", "Name": "Linda"}]

# Write csv file
with open(r'C:\Users\xa392d\Desktop\python-notes\FileIO_practice\csv_test.csv','w') as writefile:

    # Set header for each column
    for col_header in list(student_list[0].keys()):
        writefile.write(str(col_header) + ", ")
    writefile.write("\n")

    # Set value for each column
    for student in student_list:
        for col_ele in list(student.values()):
            writefile.write(str(col_ele) + ", ")
        writefile.write("\n")

# Print out the result csv
with open(r'C:\Users\xa392d\Desktop\python-notes\FileIO_practice\csv_test.csv','r') as testwritefile:
    print(testwritefile.read())
```
*Output*
```
Student ID, Gender, Name, 
1, F, Emma, 
2, M, John, 
3, F, Linda, 
```
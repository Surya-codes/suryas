

## Raw strings


Raw strings allows us to interpret strings literally.

You can denote a string as raw string by prefixing `r` before the string. Like shown below 

```python
print(R'This is a raw string. ----> \n will not become a new line')
```
*Output*
```
This is a raw string. ----> \n will not become a new line
```
### Python Raw String and Quotes


When an 'r' or 'R' prefix is present, a character following a backslash is included in the string without change, and all backslashes are left in the string.

So any character following a backslash is part of raw string. Once parser enters a raw string (non Unicode one) and encounters a backslash it knows there are 2 characters (a backslash and a char following it).

- `r'abc\d'` *comprises* a, b, c, \, d  
- `r'abc\'d'` *comprises* a, b, c, \, ', d  
- `r'abc\''` *comprises* a, b, c, \, '
- `r'abc\'` comprises a, b, c, \, ' but there is no terminating quote now.

[Reference](https://stackoverflow.com/a/19654184/6860452)  

```python
print(r'\') # invalid
```

```python
print(r'\\\') # invalid
```

```python
print(r'\'') # valid
```

## Regex
Regular expressions are a powerful language for matching text patterns. Python provides `re` module for Regex support.

```python
import re
```

Let's use below strings to perform some Regex matches.

```python
strSearchText = '''abcdefghijklmnopqurtuvwxyz
ABCDEFGHIJKLMNOPQRSTUVWXYZ
1234567890

Ha HaHa

MetaCharacters (Need to be escaped):
.[{()\^$|?*+

boeing.com

321-555-4321
123.555.1234
123*555*6000
800-555-6756
900-324-1111

Mr. Till
Mr Smith
Ms Davis
Mrs. Bodinson
Mr. B

cat
mat
pat
fat
bat
'''

sentence = 'Start a sentence and then bring it to an end'
```

To write a pattern to search for, let's use `re.compile()` method to define our patterns. We can store the pattern in a variable and reuse it later.

```python
pattern = re.compile(r'abc') # Literal string search

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
*Output*
```
<re.Match object; span=(0, 3), match='abc'>
```
As you can see `.finditer` method on pattern returns all possible matches. It is one of the easiest method to use for text pattern searches. It returns location of the match with `span` tuple.  

Observations from above match:

- The match was for `abc`, hence the `ABC` was not returned.
- The match was for `abc`, if the pattern was 'bca` then there would be no match

**Meta chacracters that must be escaped:**
`.`,`[`,`{`,`(`,`)`,`\`,`^`,`$`,`|`,`?`,`*`,`+`

```python
pattern = re.compile(r'.') 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
The above script returns everything as `.` is a special character in Regex that returns everything. We can get matches for `.` alone by escaping it with a backslash like below.
```python
pattern = re.compile(r'\.') 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
*Output*
```python
<re.Match object; span=(112, 113), match='.'>
<re.Match object; span=(132, 133), match='.'>
<re.Match object; span=(154, 155), match='.'>
<re.Match object; span=(158, 159), match='.'>
<re.Match object; span=(167, 168), match='.'>
<re.Match object; span=(195, 196), match='.'>
<re.Match object; span=(208, 209), match='.'>
```

Using Literal strings for patterns is not that useful. Where Regex shines is using the above meta characters to form patterns which will search for matches. 


| Characters |            Search Function           |
|:----------:|:------------------------------------:|
|      .     |     Any Character Except New Line    |
|     \d     |              Digit (0-9)             |
|     \D     |           Not a Digit (0-9)          |
|     \w     |   Word Character (a-z, A-Z, 0-9, _)  |
|     \W     |         Not a Word Character         |
|     \s     |   Whitespace (space, tab, newline)   |
|     \S     | Not Whitespace (space, tab, newline) |
|     \b     |             Word Boundary            |
|     \B     |          Not a Word Boundary         |
|      ^     |         Beginning of a String        |
|      $     |            End of a String           |
|     []     |   Matches Characters in brackets   |
|    [^ ]    | Matches Characters NOT in brackets |
|     \|     |              Either Or             |
|     ( )    |                Group               |

**Quantifiers**  

| Characters |           Search Function           |
|:----------:|:-----------------------------------:|
|      *     |              0 or More              |
|      +     |              1 or More              |
|      ?     |               0 or One              |
|     {3}    |             Exact Number            |
|    {3,4}   | Range of Numbers (Minimum, Maximum) |

### Examples
**See if `Start` is at the beginning of the string `sentence`**
```python
pattern = re.compile(r'^Start')

matches = pattern.finditer(sentence)

for match in matches:
    print(match)
```
*Output*
```
<re.Match object; span=(0, 5), match='Start'>
```

**See if `end` is at the end of the string `sentence`**
```python
pattern = re.compile(r'end$')

matches = pattern.finditer(sentence)

for match in matches:
    print(match)
```
*Output*
```
<re.Match object; span=(41, 44), match='end'>
```
**Searching for Phone Numbers in `strSearchText` which are in similar format to `321-555-4321`**
```python
pattern = re.compile(r'\d\d\d.\d\d\d.\d\d\d\d') 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
*Output*
```
<re.Match object; span=(138, 150), match='321-555-4321'>
<re.Match object; span=(151, 163), match='123.555.1234'>
<re.Match object; span=(164, 176), match='123*555*6000'>
<re.Match object; span=(177, 189), match='800-555-6756'>
<re.Match object; span=(190, 202), match='900-324-1111'>
```

**Searching for Phone Numbers in text file which are in similar format to `321-555-4321`**
> **Note:** data.txt file is available in this repository [here](Regular-Expressions/data.txt)
```python
with open('Regular-Expressions/data.txt','r') as f:
    contents = f.read()
    
    pattern = re.compile(r'\d\d\d.\d\d\d.\d\d\d\d')
    matches = pattern.finditer(contents)
    
    for match in matches:
        print(match)
```
*Patial Output*
```
<re.Match object; span=(12, 24), match='615-555-7164'>
<re.Match object; span=(102, 114), match='800-555-5669'>
<re.Match object; span=(191, 203), match='560-555-5153'>
<re.Match object; span=(281, 293), match='900-555-9340'>
<re.Match object; span=(378, 390), match='714-555-7405'>
<re.Match object; span=(467, 479), match='800-555-6771'>
<re.Match object; span=(557, 569), match='783-555-4799'>
<re.Match object; span=(647, 659), match='516-555-4615'>
<re.Match object; span=(740, 752), match='127-555-1867'>
<re.Match object; span=(829, 841), match='608-555-4938'>
<re.Match object; span=(915, 927), match='568-555-6051'>
<re.Match object; span=(1003, 1015), match='292-555-1875'>
<re.Match object; span=(1091, 1103), match='900-555-3205'>
<re.Match object; span=(1180, 1192), match='614-555-1166'>
```
**Using a character set `[]` to match phone numbers which have `-` or `.` in `strSearchText`**
> **Note:** You need not escape `.` in a character set `[]`  

> **Note** When you use character sets, they will match only one character regardless of how many options you specify
```python
pattern = re.compile(r'\d\d\d[-.]\d\d\d[-.]\d\d\d\d') 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
**Output**
```
<re.Match object; span=(138, 150), match='321-555-4321'>
<re.Match object; span=(151, 163), match='123.555.1234'>
```

**Using a character set `[]` to match phone numbers which begin with `800` or `900`  and have `-` or `.` in `strSearchText`**

```python
pattern = re.compile(r'[89]00[-.]\d\d\d[-.]\d\d\d\d') 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
**Output**
```
<re.Match object; span=(177, 189), match='800-555-6756'>
<re.Match object; span=(190, 202), match='900-324-1111'>
```

**Important:** When you use `^` in a character set it negates the character set i.e, it will return results which do not have the characters in the character set.  
When you run below code you will get everything which doesn't match the ranges a-z and A-Z.

```python
pattern = re.compile(r'[^a-zA-z]') 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
*Partial Output*
```
<re.Match object; span=(26, 27), match='\n'>
<re.Match object; span=(53, 54), match='\n'>
<re.Match object; span=(54, 55), match='1'>
<re.Match object; span=(55, 56), match='2'>
<re.Match object; span=(56, 57), match='3'>
<re.Match object; span=(57, 58), match='4'>
<re.Match object; span=(58, 59), match='5'>
<re.Match object; span=(59, 60), match='6'>
<re.Match object; span=(60, 61), match='7'>
<re.Match object; span=(61, 62), match='8'>
<re.Match object; span=(62, 63), match='9'>
<re.Match object; span=(63, 64), match='0'>
```
**Return matches for 'cat','mat','pat','fat' but not 'bat'**
```python
pattern = re.compile(r'[^b]at') 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
*Output*
```
<re.Match object; span=(251, 254), match='cat'>
<re.Match object; span=(255, 258), match='mat'>
<re.Match object; span=(259, 262), match='pat'>
<re.Match object; span=(263, 266), match='fat'>
```
### Using Quantifiers
**With quantifiers search for Phone Numbers in `strSearchText` which are in similar format to `321-555-4321`**
```python
pattern = re.compile(r'\d{3}.\d{3}.\d{4}') 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
*Output*
```
<re.Match object; span=(138, 150), match='321-555-4321'>
<re.Match object; span=(151, 163), match='123.555.1234'>
<re.Match object; span=(164, 176), match='123*555*6000'>
<re.Match object; span=(177, 189), match='800-555-6756'>
<re.Match object; span=(190, 202), match='900-324-1111'>
```

**a) Write a Regex to return below output**
```
<re.Match object; span=(204, 212), match='Mr. Till'>
<re.Match object; span=(213, 221), match='Mr Smith'>
<re.Match object; span=(245, 250), match='Mr. B'>
```
**Code**
```python
pattern = re.compile(r'Mr\.?\s[A-Z]\w*') 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```

**b) Write a Regex to return below output**
```
<re.Match object; span=(204, 212), match='Mr. Till'>
<re.Match object; span=(213, 221), match='Mr Smith'>
<re.Match object; span=(222, 230), match='Ms Davis'>
<re.Match object; span=(231, 244), match='Mrs. Bodinson'>
<re.Match object; span=(245, 250), match='Mr. B'>
```
**Code**
```python
pattern = re.compile(r'M(r|s|rs)\.?\s[A-Z]\w*') # Using group 

matches = pattern.finditer(strSearchText)

for match in matches:
    print(match)
```
#### Working with email addresses
**Example - 1**
```python
with open('Regular-Expressions/emails.txt','r') as f:
    contents = f.read()
    
    pattern = re.compile(r'[a-zA-Z]+@[a-zA-Z]+\.com')
    matches = pattern.finditer(contents)
    
    for match in matches:
        print(match)
```
*Ouptut*
```
<re.Match object; span=(1, 21), match='VinceFlynn@gmail.com'>
```
**Example - 2**
```python
with open('Regular-Expressions/emails.txt','r') as f:
    contents = f.read()
    
    pattern = re.compile(r'[a-zA-Z.]+@[a-zA-Z]+\.(com|edu)')
    matches = pattern.finditer(contents)
    
    for match in matches:
        print(match)
```
*Output*
```
<re.Match object; span=(1, 21), match='VinceFlynn@gmail.com'>
<re.Match object; span=(22, 48), match='Vince.Flynn@university.edu'>
```
**Example - 3**
```python
with open('Regular-Expressions/emails.txt','r') as f:
    contents = f.read()
    
    pattern = re.compile(r'[a-zA-Z0-9.-]+@[a-zA-Z-]+\.(com|edu|net)')
    matches = pattern.finditer(contents)
    
    for match in matches:
        print(match)
```
*Output*
```
<re.Match object; span=(1, 21), match='VinceFlynn@gmail.com'>
<re.Match object; span=(22, 48), match='Vince.Flynn@university.edu'>
<re.Match object; span=(49, 76), match='Vince-321-flynn@my-work.net'>
```
# Capture information from groups
```python
urls = '''
http://www.google.com
https://boeing.com
https://youtube.com
https://www.nasa.gov
'''
pattern = re.compile(r'https?://(www\.)?(\w+)(\.\w+)')



matches = pattern.finditer(urls)



for match in matches:
    print(match.group(0)) # Entire match
    print(match.group(1))
    print(match.group(2))
    print(match.group(3))
    
# Substituting matched data with groups

subbed_urls = pattern.sub(r'\2\3',urls)


print("\nSubbed URLS","\n",subbed_urls)
```
*Output*
```
http://www.google.com
www.
google
.com
https://boeing.com
None
boeing
.com
https://youtube.com
None
youtube
.com
https://www.nasa.gov
www.
nasa
.gov

Subbed URLS 
 
google.com
boeing.comcdxc
youtube.com
nasa.gov
```

### Some more methods 
We have been using `finditer` method all along. It does the best job overall. `re` module also has other methods. Take your time and go through the doucmentation to see what they do

#### `findall`
#### `match`
#### `search`
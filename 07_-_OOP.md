# Object Oriented Programming

Till now we have written our scripts/code using functions and set of statements to manipulate data. This is called procedural programming.
As the program grows, maintainability and redability of the code decreases.

This is where object oriented programming comes in. OOP allows us to group data and functions inside an object. These objects act like a blueprint for the instances we create. Let's look at below example.

```python
class Figure:  # Class
   # Initializer or Constructor
    def __init__(self):
        pass


new_figure = Figure()
new_figure.model = "737Max"
new_figure.revision = "89"
new_figure.figure_no = "23-56-78-89"

print(f"{new_figure.model}-{new_figure.revision}-Fig:{new_figure.figure_no}")
```
*Output*
```
737Max-89-Fig:23-56-78-89
```

In the above code we have created a `class` named `Figure`. For now this class doesn't have any methods (functions) or attributes (variables). 

We also created a instance of this class and stored it in `new_figure`. We added attributes like `model`,`revision`,`figure_no` to this instance.

There is a problem with this approach. Everytime we create a new instance of `Figure` we will be repeating a lot of code. This goes against **Do not repeat yourself** ideal of Python programming. 

So let's rewrite our code as shown below:

```python
class Figure:  # Class

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Attributes
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no


figure_1 = Figure(3737, 37, 809, "IPC", "23-45-67-89")
figure_2 = Figure(3737, 38, 901, "IPC", "23-45-89-23A")

print(figure_1.__dict__)
print(f"{figure_1.model}-{figure_1.revision}-Fig:{figure_1.figure_no}\n")

print(figure_2.__dict__)
print(f"{figure_2.model}-{figure_2.revision}-Fig:{figure_2.figure_no}\n")
```
*Output*
```
{'model': 3737, 'revision': 37, 'line_number': 809, 'manual': 'IPC', 'figure_no': '23-45-67-89'}
3737-37-Fig:23-45-67-89

{'model': 3737, 'revision': 38, 'line_number': 901, 'manual': 'IPC', 'figure_no': '23-45-89-23A'}
3737-38-Fig:23-45-89-23A
```

The properties that all `Figure` objects must have are defined in a method called `.__init__()`. Every time a new `Figure` object is created, `.__init__()` sets the initial state of the object by assigning the values of the object’s properties. That is, `.__init__()` initializes each new instance of the class.

You can give ``.__init__()`` any number of parameters, but the first parameter will always be a variable called `self`. When a new class instance is created, the instance is automatically passed to the self parameter in `.__init__()` so that new attributes can be defined on the object.

In the body of `.__init__()`, there are five statements using the self variable:

- self.model = Creates an attribute called `model` and assigns to it the value of the `model parameter`.
- self.revision = Creates an attribute called `revision` and assigns to it the value of the `revision parameter`.
- self.line_number = Creates an attribute called `line_number` and assigns to it the value of the `line_number parameter`.
- self.manual = Creates an attribute called `manual` and assigns to it the value of the `manual parameter`.
- self.figure_no = Creates an attribute called `figure_no` and assigns to it the value of the `figure_no parameter`.

Attributes created in ``.__init__()`` are called `instance attributes`. An instance attribute’s value is specific to a particular instance of the class. All `Figure` objects have a `model`, `revison`, `line_number`, `manual` and `figure_no` but these values will vary depending on the `Figure` instance.

Above approach will remove a lot of repetitive code. 

We can access the attributes of the instance and can change their values anytime.

```python
class Figure:  # Class

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Attributes
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no


figure_1 = Figure(3737, 37, 809, "IPC", "23-45-67-89")
figure_2 = Figure(3737, 38, 901, "IPC", "23-45-89-23A")


def print_log(*args):
    for figure in args:
        print(figure.__dict__)
        print(f"{figure.model}-{figure.revision}-Fig:{figure.figure_no}\n")


print_log(figure_1, figure_2)

figure_1.line_number = 675 # Value Changed
figure_2.manual = "AMM" # Value Changed

print_log(figure_1, figure_2)
```
*Output*
```
Before Change
{'model': 3737, 'revision': 37, 'line_number': 809, 'manual': 'IPC', 'figure_no': '23-45-67-89'}
3737-37-Fig:23-45-67-89

{'model': 3737, 'revision': 38, 'line_number': 901, 'manual': 'IPC', 'figure_no': '23-45-89-23A'}
3737-38-Fig:23-45-89-23A

After Change
{'model': 3737, 'revision': 37, 'line_number': 675, 'manual': 'IPC', 'figure_no': '23-45-67-89'}
3737-37-Fig:23-45-67-89

{'model': 3737, 'revision': 38, 'line_number': 901, 'manual': 'AMM', 'figure_no': '23-45-89-23A'}
3737-38-Fig:23-45-89-23A
```

We can also define `class attributes`. These attributes have the same value for all class instances. You can define a class attribute by assigning a value to a variable name outside of .__init__().

```python
class Figure:  # Class
    # Class Variables
    itar = False
    effort = 4

    count_of_figures = 0

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Variables
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no
        Figure.count_of_figures += 1


figure_1 = Figure(3737, 37, 809, "IPC", "23-45-67-89")
figure_2 = Figure(3737, 38, 901, "IPC", "23-45-89-23A")

def print_log(remarks, *args):
    print(remarks)
    for figure in args:
        print(figure.__dict__)
        print(f"ITAR: {figure.itar} | hours_estimated: {figure.effort}")


print_log("Class attributes applied", figure_1, figure_2)
```
*Output*
```
Class attributes applied
{'model': 3737, 'revision': 37, 'line_number': 809, 'manual': 'IPC', 'figure_no': '23-45-67-89'}
ITAR: False | hours_estimated: 4
{'model': 3737, 'revision': 38, 'line_number': 901, 'manual': 'IPC', 'figure_no': '23-45-89-23A'}
ITAR: False | hours_estimated: 4
```

## Definiing Regular methods inside Class

You can define additional methods other than `.__init__()` in class. These methods must have `self` as first paramater.

```python
class Figure:  # Class
    # Class attributes
    itar = False
    effort = 4

    count_of_figures = 0

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance attributes
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no
        Figure.count_of_figures += 1

    def book(self):  # Regular Method takes instace as first argument
        book_id = f'{self.manual}_{self.model}_{self.revision}'
        return book_id


figure_1 = Figure(3737, 37, 809, "IPC", "23-45-67-89")
figure_2 = Figure(3737, 38, 901, "IPC", "23-45-89-23A")

# Calling Regular methods
book_figure_1 = figure_1.book() # Callign the regular method
print(book_figure_1)
```
*Output*
```
IPC_3737_37
```

## Class Methods
Instead of accepting a `self` parameter, class methods take a `cls` parameter that points to the class—and not the object instance—when the method is called.

Because the class method only has access to this `cls` argument, it can’t modify object instance. That would require access to `self`. However, class methods can still modify class state that applies across all instances of the class.

Class methods are are defined by specifying `@classmethod` decorator before the method.

```python
class Figure:  # Class
    # Class Attributes
    itar = False
    effort = 4

    count_of_figures = 0

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Attributes
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no
        Figure.count_of_figures += 1

    def book(self):  # Instance Method takes instance as first argument
        book_id = f'{self.manual}_{self.model}_{self.revision}'
        return book_id

    @classmethod  # Class method takes class as first variable
    def change_itar(cls, itar_value):
        cls.itar = itar_value

    @classmethod  # Class method takes class as first variable
    def from_id(cls, fig_id):  # Factory function
        model, revision, line_number, manual, figure_no = fig_id.split('_',4)
        return cls(model, revision, line_number, manual, figure_no)


figure_1 = Figure(3737, 37, 809, "IPC", "23-45-67-89")
figure_2 = Figure(3737, 38, 901, "IPC", "23-45-89-23A")

Figure.change_itar(True)

figure_3 = Figure.from_id("3737-67-900-IPC-54-67-89-90")

print(figure_1.itar, "\n")
print(figure_3.__dict__)
```
*Ouptut*
```
True 

{'model': '3737', 'revision': '67', 'line_number': '900', 'manual': 'IPC', 'figure_no': '54-67-89-90'}
```
We can use class methods to create an instance of the class itself. `from_id` in class method demonstrates that.

## Static methods

A static method is similar to a class method, but won't get the class object as an automatic parameter. It is created by using the `@staticmethod` decorator.

It is merely attached for convenience to the class object. It could optionally be a separate function in another module.

Let's bind a static method to `Figure` class to generate random figure numbers.

```python
from random import randint


class Figure:  # Class
    # Class Attributes
    itar = False
    effort = 4

    count_of_figures = 0

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Attributes
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no
        Figure.count_of_figures += 1

    def book(self):  # Instance Method takes instance as first argument
        book_id = f'{self.manual}_{self.model}_{self.revision}'
        return book_id

    @classmethod  # Class method takes class as first variable
    def change_itar(cls, itar_value):
        cls.itar = itar_value

    @classmethod  # Class method takes class as first variable
    def from_id(cls, fig_id):
        model, revision, line_number, manual, figure_no = fig_id.split('_',4)
        return cls(model, revision, line_number, manual, figure_no)

    @staticmethod  # Static method doesn't refer to class or instance and won't operate on them
    def random_figure(count):
        collections = []
        for i in range(count):
            collections.append(f'{str(randint(0, 99))}'.zfill(2))
        rand_figure = '-'.join(collections)
        return rand_figure


figure_1 = Figure(3737, 37, 809, "IPC", "23-45-67-89")
figure_2 = Figure(3737, 38, 901, "IPC", "23-45-89-23A")

# Using class method to create an instance - Factory method
figure_3 = Figure.from_id("3737-67-900-IPC-54-67-89-90")


# Using random_figure() static method in Figure to create a random figure
figure_4 = Figure(8787, 89, 34, "IPC", Figure.random_figure(4))

print(figure_4.__dict__)
```

*Output*

```
{'model': 8787, 'revision': 89, 'line_number': 34, 'manual': 'IPC', 'figure_no': '87-97-38-98'}
```

## Inheritance - Creating Subclasses
Inheritance allows us to define a class that inherits all the methods and properties from another class.

`Parent class` is the class being inherited from, also called `base class`.

`Sub class` is a class that inherits from another class, also called derived class or `Child Class`.

Let's create a `CorrectionFigure` sub-class to inherit properties from `Figure`.This sub-class will be used to add figures for correction.

```python
from random import randint


class Figure:  # Class
    # Class Attributes
    itar = False
    effort = 4

    count_of_figures = 0

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Attributes
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no
        Figure.count_of_figures += 1

    def book(self):  # Instance Method takes instance as first argument
        book_id = f'{self.manual}_{self.model}_{self.revision}'
        return book_id

    @classmethod  # Class method takes class as first variable
    def change_itar(cls, itar_value):
        cls.itar = itar_value

    @classmethod  # Class method takes class as first variable
    def from_id(cls, fig_id):
        model, revision, line_number, manual, figure_no = fig_id.split('_',4)
        return cls(model, revision, line_number, manual, figure_no)

    @staticmethod  # Static method doesn't refer to class or instance and won't operate on them
    def random_figure(count):
        collections = []
        for i in range(count):
            collections.append(f'{str(randint(0, 99))}'.zfill(2))
        rand_figure = '-'.join(collections)
        return rand_figure


class CorrectionFigure(Figure):  # Sub Class
    effort = 1

    def __init__(self, model, revision, line_number, manual, figure_no, reason):
        super().__init__(model, revision, line_number, manual, figure_no)
        self.reason = reason


figure_1 = Figure(3737, 37, 809, "IPC", "23-45-67-89")
figure_2 = Figure(3737, 38, 901, "IPC", "23-45-89-23A")

# Using class method to create an instance - Factory method
figure_3 = Figure.from_id("3737-67-900-IPC-54-67-89-90")


# Using random_figure() static method in Figure to create a random figure
figure_4 = Figure(8787, 89, 34, "IPC", Figure.random_figure(4))

# Using sub-class to create and instance
figure_5 = CorrectionFigure("W777", 54, 809, "IPC",
                            "23-56-56-90", "Quantity Error")

print(figure_5.__dict__)
```
*Output*
```
{'model': 'W777', 'revision': 54, 'line_number': 809, 'manual': 'IPC', 'figure_no': '23-56-56-90', 'reason': 'Quantity Error'}
```
Now lets create another sub-class `RelatedFigure` to inherit from `Figure`. We will use this class to group together similar figures. Let's add few instance methods to this class for adding figures, removing figures, printing figures list.

```python
from random import randint


class Figure:  # Class
    # Class Variables
    itar = False
    effort = 4

    count_of_figures = 0

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Variables
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no
        Figure.count_of_figures += 1

    def book(self):  # Regular Method takes instace as first argument
        book_id = f'{self.manual}_{self.model}_{self.revision}'
        return book_id

    @classmethod  # Class method takes class as first variable
    def change_itar(cls, itar_value):
        cls.itar = itar_value

    @classmethod  # Class method takes class as first variable
    def from_id(cls, fig_id):
        model, revision, line_number, manual, figure_no = fig_id.split('_',4)
        return cls(model, revision, line_number, manual, figure_no)

    @staticmethod  # Static method doesn't refer to class or instance and won't operate on them
    def random_figure(count):
        collections = []
        for i in range(count):
            collections.append(f'{str(randint(0, 99))}'.zfill(2))
        rand_figure = '-'.join(collections)
        return rand_figure


class CorrectionFigure(Figure):  # Sub Class
    effort = 1

    def __init__(self, model, revision, line_number, manual, figure_no, reason):
        super().__init__(model, revision, line_number, manual, figure_no)
        self.reason = reason


class RelatedFigures(Figure):  # Sub Class
    def __init__(self, model, revision, line_number, manual, figure_no, figures=None):
        super().__init__(model, revision, line_number, manual, figure_no)
        if figures == None:
            self.figures = []
        else:
            self.figures = figures

    def add_fig(self, fig):
        if fig not in self.figures:
            self.figures.append(fig)

    def remove_fig(self, fig):
        if fig in self.figures:
            self.figures.remove(fig)

    def print_figs(self):
        for fig in self.figures:
            print('--->', fig.figure_no)

figure_1 = Figure("3737", 54, 809, "IPC", "23-34-56-78")  # Instance
figure_2 = Figure("3737", 54, 809, "IPC", "23-34-56-90")  # Instance
# Instance created using class method
figure_3 = Figure.from_id("3737-67-900-IPC-54-67-89-90")
# Instance using child class
figure_4 = CorrectionFigure("3737", 54, 809, "IPC", "23-56-56-90", "PLD")
# Instance using Child class
relatedFigure1 = RelatedFigures(
    "3737", 54, 809, "IPC", "23-56-56-78",[figure_1,figure_2])

relatedFigure1.add_fig(figure_3) # Add figure
relatedFigure1.remove_fig(figure_1) # Remove figure


relatedFigure1.effort = 10

print(relatedFigure1)
print(relatedFigure1.__dict__)
```
*Output*
```
<__main__.RelatedFigures object at 0x000001604E9A9DF0>
{'model': '3737', 'revision': 54, 'line_number': 809, 'manual': 'IPC', 'figure_no': '23-56-56-78', 'figures': [<__main__.Figure object at 0x000001604E9A9220>, <__main__.Figure object at 0x000001604E9A91F0>], 'effort': 10}
```

As you've observed, when you try to print the instance or related instances, Python is giving the object and the memory location. We should make that human friendly. It’s common practice in Python to provide a [string representation](https://docs.python.org/3/reference/datamodel.html) of your object for the consumer of your class (a bit like API documentation). There are two ways to do this using dunder methods:

- `__repr__`: The "official" string representation of an object. This is how you would make an instance object of the class. The goal of __repr__ is to be unambiguous.

- `__str__`: The "informal" or nicely printable string representation of an object. This is for the enduser.

We'll also implement __add__ to sum of the effort values for all related figures.

Let's implement them in our code.

```python
from random import randint


class Figure:  # Class
    # Class Variables
    itar = False
    effort = 4

    count_of_figures = 0

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Variables
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no
        Figure.count_of_figures += 1

    def book(self):  # Regular Method takes instace as first argument
        book_id = f'{self.manual}_{self.model}_{self.revision}'
        return book_id

    @classmethod  # Class method takes class as first variable
    def change_itar(cls, itar_value):
        cls.itar = itar_value

    @classmethod  # Class method takes class as first variable
    def from_id(cls, fig_id):
        model, revision, line_number, manual, figure_no = fig_id.split('_',4)
        return cls(model, revision, line_number, manual, figure_no)

    @staticmethod  # Static method doesn't refer to class or instance and won't operate on them
    def random_figure(count):
        collections = []
        for i in range(count):
            collections.append(f'{str(randint(0, 99))}'.zfill(2))
        rand_figure = '-'.join(collections)
        return rand_figure

    def __repr__(self):
        return(f"Figure({self.model},{self.revision},{self.line_number},{self.manual},{self.figure_no})")
    
    def __str__(self):
        return(f"{self.model},{self.revision},{self.line_number},{self.manual},{self.figure_no}")


class CorrectionFigure(Figure):  # Sub Class
    effort = 1

    def __init__(self, model, revision, line_number, manual, figure_no, reason):
        super().__init__(model, revision, line_number, manual, figure_no)
        self.reason = reason


class RelatedFigures(Figure):  # Sub Class
    def __init__(self, model, revision, line_number, manual, figure_no, figures=None):
        super().__init__(model, revision, line_number, manual, figure_no)
        if figures == None:
            self.figures = []
        else:
            self.figures = figures

    def add_fig(self, fig):
        if fig not in self.figures:
            self.figures.append(fig)

    def remove_fig(self, fig):
        if fig in self.figures:
            self.figures.remove(fig)

    def print_figs(self):
        for fig in self.figures:
            print('--->', fig.figure_no)

# Figure.change_itar(True)
# figure1.itar = False
# figure2.effort = 8


figure_1 = Figure("3737", 54, 809, "IPC", "23-34-56-78")  # Instance
figure_2 = Figure("3737", 54, 809, "IPC", "23-34-56-90")  # Instance
# Instance created using class method
figure_3 = Figure.from_id("3737-67-900-IPC-54-67-89-90")
figure_4 = CorrectionFigure("3737", 54, 809, "IPC", "23-56-56-90", "PLD")
relatedFigure1 = RelatedFigures(
    "3737", 54, 809, "IPC", "23-56-56-78",[figure_1,figure_2])

relatedFigure1.add_fig(figure_3)
relatedFigure1.remove_fig(figure_1)


relatedFigure1.effort = 10

print(relatedFigure1)
print(relatedFigure1.__dict__)
```
*Output*
```
3737,54,809,IPC,23-56-56-78
{'model': '3737', 'revision': 54, 'line_number': 809, 'manual': 'IPC', 'figure_no': '23-56-56-78', 'figures': [Figure(3737,54,809,IPC,23-34-56-90), Figure(3737,67,900,IPC,54-67-89-90)], 'effort': 10}
```

The output looks much better now. 
- `__repr__` returns `Figure(3737,67,900,IPC,54-67-89-90`
- `__str__` returns `3737,54,809,IPC,23-56-56-78`

## Property Decorators

For sake of simplicity, we'll just start with below code.

```python
class Figure:  # Class

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Variables
        self.model = model
        self.revision = str(revision)
        self.line_number = str(line_number)
        self.manual = manual
        self.figure_no = figure_no
        self.id = f"{self.model}_{self.revision}_{self.line_number}_{self.manual}_{self.figure_no}"

figure1 = Figure("3737", 54, 809, "IPC", "23-34-56-78")
print(figure1.id)
```
*Output*
```output
3737_54_809_IPC_23-34-56-78
```
Now let's change the value of manual to **ZIPL** and see the output again.

```python
class Figure:
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Attributes
        self.model = model
        self.revision = str(revision)
        self.line_number = str(line_number)
        self.manual = manual
        self.figure_no = figure_no
        self.id = f"{self.model}_{self.revision}_{self.line_number}_{self.manual}_{self.figure_no}"


figure1 = Figure("3737", 54, 809, "IPC", "23-34-56-78")
figure1.manual = 'ZIPL'
print(figure1.id)
```
*Output*
```
3737_54_809_IPC_23-34-56-78
```
What happened? The Instance attributes are set during creation of the instance. When you change an instance attribute later, any dependent attributes might not get updated.

We could use a method to generate the id but that would require any users of our class to update their code and certainly their existing code will be unreliable.

This is where `@property` decorator comes in handy. It is one of the built-in decorators. The main purpose of `@property` decorator is to change class methods or attributes in such a way so that the user of the class need not make any change in their code. We can use `@property` over a method to fix the `id` issue in our code. This is called `getter` method.

```python
class Figure:  # Class

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Variables
        self.model = model
        self.revision = str(revision)
        self.line_number = str(line_number)
        self.manual = manual
        self.figure_no = figure_no
        # Removed id from here
        
    # @property decorator makes sure id updates everytime something has changed. 
    @property
    def id(self):
        return f"{self.model}_{self.revision}_{self.line_number}_{self.manual}_{self.figure_no}"


figure1 = Figure("3737", 54, 809, "IPC", "23-34-56-78")

print(figure1.id)

figure1.manual = "ZIPL"

print("\nAfter Change\n")
print(figure1.id)
```
*Output*
```
3737_54_809_IPC_23-34-56-78

After Change

3737_54_809_ZIPL_23-34-56-78
```
### Defining `setter` and `deleter` methods with `@property` Decorator 
Similar to the getter method(we have defined in the previous example) we can also define setter and deleter methods for any attribute which is using `@property` in Python.

The setter method will set the value of the attribute and the deleter method will delete the attribute from the memory.

Let's implement a `setter` and `deleter` method for our fullname attribute. For defining a `setter` and `deleter` method, for an attribute with `@property` decorator set on its `getter` method, we use a special syntax, where we put `@ATTRIBUTE_NAME.setter` and `@ATTRIBUTE_NAME.deleter` decorator on method with name same as the ATTRIBUTE_NAME. This is shown in the below example:

```python
class Figure:  # Class

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Variables
        self.model = model
        self.revision = str(revision)
        self.line_number = str(line_number)
        self.manual = manual
        self.figure_no = figure_no
        self._extension_days = 4

        # Removed id from here
        
    # @property decorator makes sure id updates everytime something has changed. 
    @property
    def id(self):
        return f"{self.model}_{self.revision}_{self.line_number}_{self.manual}_{self.figure_no}"

    @property
    def extension_days(self):
        print("Getter Method called")
        return self._extension_days
    @extension_days.setter
    def extension_days(self,days):
        if (days>7):
            raise ValueError("Extension cannot be given beyond 6 days")
        print("Setter Method called")
        self._extension_days = days

figure1 = Figure("3737", 54, 809, "IPC", "23-34-56-78")

figure1.extension_days
print("-" * 50)
figure1.extension_days = 6

print(figure1.__dict__)
print("-" * 50)

del figure1.extension_days

print(figure1.__dict__)
```
*Output*
```
print(figure1.__dict__)

Getter Method called
--------------------------------------------------
Setter Method called
{'model': '3737', 'revision': '54', 'line_number': '809', 'manual': 'IPC', 'figure_no': '23-34-56-78', '_extension_days': 6}
--------------------------------------------------
Caution! This will remove extensions days
{'model': '3737', 'revision': '54', 'line_number': '809', 'manual': 'IPC', 'figure_no': '23-34-56-78', '_extension_days': None}

```

## Rewriting our code

Now let's rewrite our `Figure` class to include getters, setters & deleters.

```python
from random import randint


class Figure:  # Class
    # Class Variables
    itar = False
    effort = 4
    count_of_figures = 0

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Variables
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no
        self._extension_days = 1
        Figure.count_of_figures += 1

    def book(self):  # Regular Method takes instace as first argument
        book_id = f"{self.manual}_{self.model}_{self.revision}"
        return book_id
    
    @classmethod  # Class method takes class as first variable
    def change_itar(cls, itar_value):
        cls.itar = itar_value

    @classmethod  # Class method takes class as first variable
    def from_id(cls, fig_id):
        model, revision, line_number, manual, figure_no = fig_id.split("_", 4)
        return cls(model, revision, line_number, manual, figure_no)

    @staticmethod  # Static method doesn't refer to class or instance and won't operate on them
    def random_figure(count):
        collections = []
        for i in range(count):
            collections.append(f"{str(randint(0, 99))}".zfill(2))
        rand_figure = "-".join(collections)
        return rand_figure

    # @property decorator makes sure id updates everytime something has changed.
    @property
    def id(self):
        return f"{self.model}_{self.revision}_{self.line_number}_{self.manual}_{self.figure_no}"

    @property # Getter
    def extension_days(self):
        print("Getter Method called")
        return self._extension_days

    @extension_days.setter # Setter
    def extension_days(self, days):
        if days > 7:
            raise ValueError("Extension cannot be given beyond 6 days")
        print("Setter Method called")
        self._extension_days = days

    @extension_days.deleter # Deleter
    def extension_days(self):
        print("Caution! This will remove extensions days")

        self._extension_days = None

    def __repr__(self):
        return f"Figure({self.model},{self.revision},{self.line_number},{self.manual},{self.figure_no})"

    def __str__(self):
        return f"{self.model},{self.revision},{self.line_number},{self.manual},{self.figure_no}"


class CorrectionFigure(Figure):  # Sub Class
    effort = 1

    def __init__(self, model, revision, line_number, manual, figure_no, reason):
        super().__init__(model, revision, line_number, manual, figure_no)
        self.reason = reason


class RelatedFigures(Figure):  # Sub Class
    def __init__(self, model, revision, line_number, manual, figure_no, figures=None):
        super().__init__(model, revision, line_number, manual, figure_no)
        if figures == None:
            self.figures = []
        else:
            self.figures = figures

    def add_fig(self, fig):
        if fig not in self.figures:
            self.figures.append(fig)

    def remove_fig(self, fig):
        if fig in self.figures:
            self.figures.remove(fig)

    def print_figs(self):
        for fig in self.figures:
            print("--->", fig.figure_no)
```

## Extra reading
### [4 pillars of OOP ](https://medium.com/swlh/the-4-pillars-of-oop-in-python-9daaca4c0d13)

- Encapsulation
- Abstraction
- Inheritance
- PolyMorphism
### [Multiple Inheritance](https://docs.python.org/3/tutorial/classes.html#multiple-inheritance)


# Errors and Exception handling

If you've come this far, you might have encountered some errors in your Python journey.

```python
print('Hello)
```
*Output*
```
File "/ipykernel_6914/1679058590.py", line 1
    print('Hello)
                 ^
SyntaxError: EOL while scanning string literal
```
Here we get an error for missing a closing quote. Python shows your the error description with the line number where the error is present. These type of errors which are detected during execution are called exception.

You can check out the full list of built-in exceptions [here](https://docs.python.org/3/library/exceptions.html). Now lets learn how to handle errors and exceptions in our own code.

## `try` and `except`

Error handling with `try` and `except` allows our code to run even if there is an error.

```python
while True:
    try: # 
        age = int(input('What is your age? '))
        print(age)

    except:
        print('Please enter a number')

    else:
        print('Thank you')
        break
```
We can prepare our error handling to respond differently to different exceptions.

```python
while True:

    try:
        age = int(input('What is your age? '))
        print(age)
        print(10/age)

    except ValueError:
        print('Please enter a number')

    except ZeroDivisionError:
        print('Please enter age higher than 0')

    else:
        print('Thank you')
        break
```
## Catching exceptions

```python
def sum(num1, num2):
    try:
        return num1+num2
    except TypeError as err:
        print(f'Please enter numbers \n{err}')


print(sum('1', 2))
```

## Catching multiple exceptions

```python
def sum(num1, num2):
    try:
        return num1+num2
    except (TypeError,ZeroDivisionError) as err:
        print(f'Please enter numbers \n{err}')


print(sum('1', 2))
```

## Raising errors

You can choose to throw an exception if a condition occurs.
To raise an exception, use the `raise` keyword.

```python
x = -1

if x < 0:
  raise Exception("Sorry, no numbers below zero") 
```

```python
while True:

    try:
        age = int(input('What is your age? '))
        print(age)
        print(10/age)
        # raise Exception('hey cut it out')
        raise ValueError('Please enter a number')

    # except ValueError:
    #     print('Please enter a number')

    except ZeroDivisionError:
        print('Please enter age higher than 0')

    else:
        print('Thank you')
        break
```

## finally

The finally: block of code will always be run regardless if there was an exception in the try code block. The syntax is:

```python
try:
   Code block here
   ...
   Due to any exception, this code may be skipped!
except ExceptionI:
   If there is ExceptionI, then execute this block.
except ExceptionII:
   If there is ExceptionII, then execute this block.
   ...
else:
   If there is no exception then execute this block.
finally:
   This code block would always be executed.
```

from random import randint


class Figure:  # Class
    # Class Variables
    itar = False
    effort = 4

    count_of_figures = 0

    # Initializer or Constructor
    def __init__(self, model, revision, line_number, manual, figure_no):
        # Instance Variables
        self.model = model
        self.revision = revision
        self.line_number = line_number
        self.manual = manual
        self.figure_no = figure_no
        self._extension_days = 1
        Figure.count_of_figures += 1

    def book(self):  # Regular Method takes instace as first argument
        book_id = f"{self.manual}_{self.model}_{self.revision}"
        return book_id

    @classmethod  # Class method takes class as first variable
    def change_itar(cls, itar_value):
        cls.itar = itar_value

    @classmethod  # Class method takes class as first variable
    def from_id(cls, fig_id):
        model, revision, line_number, manual, figure_no = fig_id.split("_", 4)
        return cls(model, revision, line_number, manual, figure_no)

    @staticmethod  # Static method doesn't refer to class or instance and won't operate on them
    def random_figure(count):
        collections = []
        for i in range(count):
            collections.append(f"{str(randint(0, 99))}".zfill(2))
        rand_figure = "-".join(collections)
        return rand_figure

    # @property decorator makes sure id updates everytime something has changed.
    @property
    def id(self):
        return f"{self.model}_{self.revision}_{self.line_number}_{self.manual}_{self.figure_no}"

    @property
    def extension_days(self):
        print("Getter Method called")
        return self._extension_days

    @extension_days.setter
    def extension_days(self, days):
        if days > 7:
            raise ValueError("Extension cannot be given beyond 6 days")
        print("Setter Method called")
        self._extension_days = days

    @extension_days.deleter
    def extension_days(self):
        print("Caution! This will remove extensions days")

        self._extension_days = None

    def __repr__(self):
        return f"Figure({self.model},{self.revision},{self.line_number},{self.manual},{self.figure_no})"

    def __str__(self):
        return f"{self.model},{self.revision},{self.line_number},{self.manual},{self.figure_no}"


class CorrectionFigure(Figure):  # Sub Class
    effort = 1

    def __init__(self, model, revision, line_number, manual, figure_no, reason):
        super().__init__(model, revision, line_number, manual, figure_no)
        self.reason = reason


class RelatedFigures(Figure):  # Sub Class
    def __init__(self, model, revision, line_number, manual, figure_no, figures=None):
        super().__init__(model, revision, line_number, manual, figure_no)
        if figures == None:
            self.figures = []
        else:
            self.figures = figures

    def add_fig(self, fig):
        if fig not in self.figures:
            self.figures.append(fig)

    def remove_fig(self, fig):
        if fig in self.figures:
            self.figures.remove(fig)

    def print_figs(self):
        for fig in self.figures:
            print("--->", fig.figure_no)
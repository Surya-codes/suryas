import Figure

# Create Figure from Methods
figure_1 = Figure.Figure("W777", 45, 506, 'IPC', '23-45-67-89')
figure_2 = Figure.Figure.from_id("7777_12_712_AMM_23-45-67-89")
figure_3 = Figure.CorrectionFigure("W777", 55, 606, 'IPC', '25-43-66-87', "Quantity Error")
figure_4 = Figure.RelatedFigures("8787", 65, 103, 'CMM', '25-43-22-11A')

figures = [figure_1, figure_2, figure_3, figure_4]


def print_figs(reason):
    print(reason,'\n')
    for figure in figures:
        print(figure.__dict__, '\n')
    print('-'*50)
    print('\n')

print_figs("Printing All Figures")

# Add Figures to figure_4
figure_4.add_fig('54-33-22-11A')

print_figs("Added figure to Figure-4")

# Test extension days

print(f'Current extension days - {figure_2.extension_days}\n')

figure_2.extension_days = 9 # anything more than 6 will raise an error

print(f'Updated extension days - {figure_2.extension_days}\n')

del figure_2.extension_days

print(f'Extension days - {figure_2.extension_days}\n')

